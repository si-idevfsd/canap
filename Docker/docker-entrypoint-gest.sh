#!/bin/sh

set -e -x

# Substitute 12-factored env vars now (we couldn't do that at compile time
# because we don't know the values until runtime).
sed -i -e "s|_REPLACE_ME_LATER__API_LOCATION|$API_LOCATION|"      \
       -e "s|_REPLACE_ME_LATER__LOGIN_REDIRECT|$LOGIN_REDIRECT|"  \
       /usr/local/apache2/htdocs/gest/js/app.*.js

exec /usr/local/bin/httpd-foreground
