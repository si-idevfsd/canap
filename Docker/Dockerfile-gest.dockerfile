FROM node:15-alpine as build
WORKDIR /home/node/app
COPY ./gest /home/node/app
RUN npm install -y  && npm run build

FROM httpd:latest 
WORKDIR /root/
COPY --from=build /home/node/app/dist  /usr/local/apache2/htdocs/gest
COPY Docker/docker-entrypoint-gest.sh /docker-entrypoint.sh
CMD /docker-entrypoint.sh