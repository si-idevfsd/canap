FROM php:8.2-apache

WORKDIR /var/www/html/

RUN apt-get update; apt-get install -yq git vim zip libldap2-dev libzip-dev msmtp msmtp-mta;
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN export PATH=$PATH:/root/.composer/vendor/bin

RUN docker-php-ext-install pdo pdo_mysql zip
RUN a2enmod rewrite

ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN echo "LimitRequestFieldSize 81920" > /etc/apache2/conf-enabled/say-yes-to-bearer-tokens.conf
# RUN sed -ri -e 's!/var/www/!q${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install ldap

# msmtp log 
RUN touch /var/log/msmtp.log && chown www-data:www-data /var/log/msmtp.log

# Copy the code into the container
COPY ./API /var/www/html/
RUN composer update && composer install

# Change permissions on copied files
RUN chown -R www-data:www-data /var/www/html/
# USER www-data
