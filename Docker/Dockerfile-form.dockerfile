FROM php:8.2-apache

RUN apt-get update;
RUN apt-get install -f -y msmtp msmtp-mta;

RUN docker-php-ext-install pdo_mysql

# msmtp log 
RUN touch /var/log/msmtp.log && chown www-data:www-data /var/log/msmtp.log

# cleanup
RUN apt-get -y autoremove && apt-get -y clean;

# Copy the config into the container
COPY ./Docker/canap_apache.conf /etc/apache2/conf-enabled

# Copy the code into the container
COPY ./form /var/www/html/
