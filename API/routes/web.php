<?php

// Add a basic home page to the API, without auth
$router->get('/', function() {
  return view('welcome');
});

// Comfort route to test DB and PDO
// https://github.com/laravel/framework/issues/30737
$router->get('/test-db', function() {
  try {
    DB::connection()->getPdo();
  } catch (\Exception $e) {
    die("<pre>Could not connect to the database.  Please check your configuration. error:" . $e );
  }
});

// Comfort route to test the status list
$router->get('/test-status', 'StatusController@getAvailableStatus');


$router->get('api/auth/login', 'AuthController@login');
$router->get('api/auth/tequilareturn', 'AuthController@tequilareturn');
// logout

$router->group(['middleware' => 'jwt.auth'], function () use ($router) {

  // Users
  $router->get('api/user', 'UsersController@getData');
  $router->get('api/user/permittedjobs', 'UsersController@getPermittedJobs');
  $router->get('api/user/hascommentedormarked', 'UsersController@getCommentedAndMarkedApplicantsByUser');
  $router->get('api/user/data/{sciper}', 'UsersController@getUserDataBySciper');
  $router->get('api/user/connection', 'UsersController@getLastConnection');
  $router->patch('api/user/connection', 'UsersController@setLastConnection');

  // Applicants
  $router->get('api/applicants', 'ApplicantsController@getAll');
  $router->get('api/applicants/job/{job}', 'ApplicantsController@getJobApplicants');
  $router->get('api/applicant/{id:[0-9]+}', 'ApplicantsController@getOneById');
  $router->get('api/applicant/{id:[0-9]+}/export', 'ApplicantsController@export');
  $router->delete('api/applicant/{id:[0-9]+}', 'ApplicantsController@delete');

  // Comments
  $router->get('api/applicant/{id:[0-9]+}/comments', 'CommentsController@getApplicantComments');
  $router->put('api/comment', 'CommentsController@create');
  $router->patch('api/comment/{id:[0-9]+}', 'CommentsController@update');
  $router->delete('api/comment/{id:[0-9]+}', 'CommentsController@delete');

  // Markers
  $router->get('api/applicant/{id:[0-9]+}/marker', 'MarkersController@getUserMarkerOnApplicant');
  $router->put('api/marker', 'MarkersController@create');
  $router->patch('api/marker/{id:[0-9]+}', 'MarkersController@update');
  $router->delete('api/marker/{id:[0-9]+}', 'MarkersController@delete');

  // Status
  $router->get('api/status', 'StatusController@getAvailableStatus');
  $router->patch('api/status/applicant/{id:[0-9]+}', 'StatusController@updateApplicantStatus');

  // Positions, jobs and locations
  $router->get('api/positions', 'PositionsController@getAll');
  $router->get('api/locations', 'PositionsController@getAvailableLocations');
  $router->get('api/jobs', 'PositionsController@getAvailableJobs');
  $router->get('api/groups', 'PositionsController@getAvailableAccessGroups');
  $router->put('api/position', 'PositionsController@createPosition');
  $router->put('api/location', 'PositionsController@createLocation');
  $router->put('api/job', 'PositionsController@createJob');
  $router->patch('api/position/{id:[0-9]+}', 'PositionsController@updatePosition');
  $router->delete('api/position/{id:[0-9]+}', 'PositionsController@deletePosition');

  // Files
  $router->get('api/file/{id:[0-9]+}', 'FilesController@getFile');
  $router->get('api/files/applicant/{id:[0-9]+}', 'FilesController@exportApplicantFiles');

  // Stats
  $router->get('api/stats/total', 'StatsController@getTotal');

  // Mails
  $router->post('api/mail/applicant', 'MailsController@mailApplicant');
  $router->get('api/mail/templates', 'MailTemplateController@showAll');
  $router->put('api/mail/templates/new', 'MailTemplateController@create');
  $router->delete('api/mail/templates/delete/{id:[0-9]+}', 'MailTemplateController@delete');
  $router->patch('api/mail/templates/update/{id:[0-9]+}', 'MailTemplateController@update');
});