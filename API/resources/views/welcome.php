<!-- View stored in resources/views/welcome.php -->
<html>
  <head>
    <title>CANAP API</title>
    <style>
      header img {
        vertical-align: middle;
      }
      header .logo-epfl img {
        height: 30px
      }
      header .logo-fa  img {
        height: 50px
      }
      </style>
  </head>
  <body>
    <header>
      <a class="logo logo-epfl">
        <img src="/epfl-logo.svg" alt="Logo EPFL, École polytechnique fédérale de Lausanne" class="img-fluid">
      </a>
      <a class="logo logo-fa">
        <img src="/FA.png" alt="Logo FA, Formation Apprentis EPFL" class="img-fluid">
      </a>
    </header>
    <h1>CANAP API</h1>
  </body>
</html>

