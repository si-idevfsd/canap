<?php

return [
  'unauthorized' => 'Action non autorisée',
  'notfound' => 'Contenu non trouvé',
  'success' => [
    'created' => [
      'marker' => 'Marqueur ajouté',
      'comment' => 'Commentaire ajouté',
      'position' => 'Offre ajoutée',
      'location' => 'Lieu ajouté',
      'job' => 'Métier ajouté'
    ],
    'updated' => [
      'marker' => 'Marqueur modifié',
      'comment' => 'Commentaire modifié',
      'position' => 'Offre modifiée',
      'location' => 'Lieu modifié',
      'job' => 'Métier modifié',
      'status' => 'Statut modifié'
    ],
    'deleted' => [
      'marker' => 'Marqueur supprimé',
      'comment' => 'Commentaire supprimé',
      'position' => 'Offre supprimée',
      'location' => 'Lieu supprimé',
      'job' => 'Métier supprimé',
      'application' => 'Candidature supprimée'
    ],
    'sent' => [
      'mail' => 'Mail envoyé avec succès'
    ]
  ],
  'error' => [
    'deleted' => [
      'position' => 'Des candidatures sont liées à cette offre, cette suppression est impossible !'
    ]
  ]
];

