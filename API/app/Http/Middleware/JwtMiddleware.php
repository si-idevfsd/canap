<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\DB;

function isSuperUser($sciper) {
  $sciperSuperUsers = getenv('SCIPERSUPERUSER');

    if ($sciperSuperUsers !== false ) {
        if (array_search($sciper, explode(",", $sciperSuperUsers))!== false) {
          return true;
        }
    } 
  return false;
}

function allUserPermissions() {
  return ["informaticienEntreprise", "informaticienSysteme", "informaticienDev", "laborantinBiologie", "laborantinChimie", "laborantinPhysique", "logisticien", "planificateurElectricien", "electronicien", "polyMecanicien", "employeCommerce", "gardienAnimaux", "interactiveMediaDesigner", "informaticien", ]; # TODO: there are more
  // return DB::table('job')
  // ->join('position', 'applicant.fk_position', '=', 'position.position_id')
  // ->join('job', 'position.fk_job', '=', 'job.job_id')
  // ->where('job_short_value', $job)
  // ->where('fk_status', 'Valide')
  // ->get();
}

class JwtMiddleware
{
  public function handle($request, Closure $next, $guard = null)
  {
    $token = str_replace('Bearer ', '', $request->headers->get('Authorization'));

    if ($token == 'null') {
      return response()->json([
        'error' => 'Connexion requise'
      ], 401);
    }
    try {
      $content = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
    } catch (ExpiredException $e) {
      return response()->json([
        'error' => 'Session expirée',
        'type' => 'expired'
      ], 400);
    } catch (Exception $e) {
      return response()->json([
        'error' => 'Error while decoding token',
        'type' => 'error'
      ], 400);
    }

    // Make sciper, data & permissions accessible through request
    $request->attributes->add(['user_sciper' => $content->tequila_data->sciper]);
    $request->attributes->add(['user_data' => $content->tequila_data]);
    $request->attributes->add(['user_permissions' => $content->permissions]);
    if (isSuperUser($content->tequila_data->sciper)) {
      $request->attributes->add(['user_permissions' => allUserPermissions()]);
    }
    $request->attributes->add(['user_role' => $content->role]);

    return $next($request);
  }
}