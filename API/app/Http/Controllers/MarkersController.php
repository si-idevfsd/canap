<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Helpers\AccessLevelHelper;
use Illuminate\Support\Facades\Lang;

class MarkersController extends Controller
{
  private $request;
  private $user_sciper;
  private $user_permissions;

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
  }

  public function getUserMarkerOnApplicant($id)
  {
    $marker = DB::table('marker')->where('fk_applicant_id', $id)->where('marker_owner_sciper', $this->user_sciper)->first();
    return $marker ? ["marker" => $marker] : ["marker" => ['marker_value' => null]];
  }

  public function create()
  {
    $this->validate($this->request, [
      'value' => 'required|numeric',
      'applicant_id' => 'required|numeric',
    ], [lang::get('validation.required')]);

    $new_value = $this->request->input('value');
    $new_applicant_id = $this->request->input('applicant_id');

    $applicant_job = DB::table('applicant')
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->where('applicant_id', $new_applicant_id)
      ->pluck('job_short_value')->first();
    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);

    if ($has_access) {

      $marker_exists = DB::table('marker')->where('fk_applicant_id', $new_applicant_id)->where('marker_owner_sciper', $this->user_sciper)->first();
      if ($marker_exists) {
        $id = $marker_exists->marker_id;
        DB::table('marker')->where('marker_id', $id)->update(['marker_value' => $new_value]);
        return ["message" => lang::get('http.success.created.marker'), "id" => $id];
  
      } else {

      $inserted_id = DB::table('marker')->insertGetId([
        "marker_owner_sciper" => $this->user_sciper,
        "marker_value" => $new_value,
        "fk_applicant_id" => $new_applicant_id
      ]);
      return ["message" => lang::get('http.success.created.marker'), "id" => $inserted_id];
        
    }
  } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function update($id)
  {
    $this->validate($this->request, [
      'value' => 'required|numeric'
    ], [lang::get('validation.required')]);

    $new_value = $this->request->input('value');
    $wanted_marker_exists = DB::table('marker')->where('marker_id', $id)->where('marker_owner_sciper', $this->user_sciper)->exists();
    if ($wanted_marker_exists) {
      DB::table('marker')->where('marker_id', $id)->update(['marker_value' => $new_value]);
      return ["message" => lang::get('http.success.updated.marker'), "id" => $id];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function delete($id)
  {
    $wanted_marker_exists = DB::table('marker')->where('marker_id', $id)->where('marker_owner_sciper', $this->user_sciper)->exists();
    if ($wanted_marker_exists) {
      DB::table('marker')->where('marker_id', $id)->where('marker_owner_sciper', $this->user_sciper)->delete();
      return ["message" => lang::get('http.success.deleted.marker')];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }
}
