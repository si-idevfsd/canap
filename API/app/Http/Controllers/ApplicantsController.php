<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\AccessLevelHelper;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;
use ZipArchive;

class ApplicantsController extends Controller
{
  private $request;
  private $user_sciper;
  private $user_permissions;
  private $user_role;

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
    $this->user_role = $this->request->get('user_role');
  }

  public function getAll()
  {
    // Récupère toutes les candidatures autorisées
    $applicants = [];

    if (AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable')) {

      $job_applicants = DB::table('applicant')
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->join('location', 'position.fk_location', '=', 'location.location_id')
      ->get();
    }

    if (AccessLevelHelper::hasPermittedRole($this->user_role, 'formateur')) {
      $job_applicants = DB::table('applicant')
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->join('location', 'position.fk_location', '=', 'location.location_id')
      ->leftJoin('marker', function ($join) {
        $join->on('applicant.applicant_id', '=', 'marker.fk_applicant_id');
        $join->where('marker_owner_sciper', $this->user_sciper);
      })
      ->where(function ($query) {
        $query->where('fk_status', 'Valide')
              ->orWhere('fk_status', 'En entretien');
    })
    ->get();

      // $job_applicants = DB::table('applicant')
      // ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      // ->join('job', 'position.fk_job', '=', 'job.job_id')
      // ->join('location', 'position.fk_location', '=', 'location.location_id')
      // ->where('fk_status', 'Valide')
      // ->get();
    }

    foreach ($job_applicants as $key => $applicant) {
      if (in_array($applicant->job_short_value, $this->user_permissions)) {
        array_push($applicants, $applicant);
      }
    }
    return $applicants;
  }

  public function getJobApplicants($job)
  {
    // Récupère toutes les candidatures d'un métier
    if (AccessLevelHelper::isJobValid($job)) {
      $has_access = AccessLevelHelper::hasAccessToJob($job, $this->user_permissions);
      if ($has_access) {
        if (AccessLevelHelper::hasPermittedRole($this->user_role, 'formateur')) {
          return DB::table('applicant')
          ->join('position', 'applicant.fk_position', '=', 'position.position_id')
          ->join('job', 'position.fk_job', '=', 'job.job_id')
          ->where('job_short_value', $job)
          ->where('fk_status', 'Valide')
          ->get();
        } else {
          return DB::table('applicant')
          ->join('position', 'applicant.fk_position', '=', 'position.position_id')
          ->join('job', 'position.fk_job', '=', 'job.job_id')
          ->where('job_short_value', $job)
          ->get();
        }
      } else {
        return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
      }
    } else {
      return response()->json(['error' => 404, 'message' => lang::get('http.notfound')], 404);
    }
  }

  public function getOneById($id)
  {
    $applicant_job = $this->getApplicantJob($id);

    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);
    if ($has_access) {
      $data = AccessLevelHelper::hasPermittedRole($this->user_role, 'formateur') ? $this->getOne($id, true) : $this->getOne($id, false);

      if ($data['personal_data'] == null) {
        return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
      } else {
        return response()->json($data, 200);
      }
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function delete($id)
  {
    $applicant_job = $this->getApplicantJob($id);
    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);
    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');
    if ($has_access && $has_permitted_role) {

      // Delete Applicant files and folder
      $applicant_files = DB::table('file')->where('fk_applicant_id', $id)->get();
      $applicant_folder = "";
      foreach ($applicant_files as $file) {
        unlink($file->file_path);
        $applicant_folder = dirname($file->file_path);
      }
      // delete applicant folder
      if ($applicant_folder != "") {
        rmdir($applicant_folder);
      }

      // Delete Applicant from DB
      DB::table('applicant')->where('applicant_id', $id)->delete();
      return ["message" => lang::get('http.success.deleted.application')];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function export($id)
  {
    // export data (json) and files in a .zip file
    $applicant_job = $this->getApplicantJob($id);
    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);
    if ($has_access) {
      $tmp_dir = sys_get_temp_dir();
      $zipFileName = $id.'export.zip';
      $zip = new ZipArchive;

      $tmp_file_path = $tmp_dir . '/' .$id .'-export.json';
      $json = AccessLevelHelper::hasPermittedRole($this->user_role, 'formateur') ? json_encode($this->getOne($id, true)) : json_encode($this->getOne($id, false));
      File::put($tmp_file_path, $json);

      $applicant_files = DB::table('file')
      ->where('fk_applicant_id', $id)->get();

      if ($zip->open($tmp_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
        foreach ($applicant_files as $file) {
          $zip->addFile($file->file_path, $file->file_name);
        }
        $zip->addFile($tmp_file_path, 'data.json');
        $zip->close();
      }

      $export_path = $tmp_dir .'/'. $zipFileName;
      return response()->download($export_path, $id .'export.zip', ['Content-Type' => 'application/zip'])->deleteFileAfterSend(true);
    }
  }

  private function getOne($id, $status_restricted)
  {
    // get applicant
    if ($status_restricted) {
      $applicant = DB::table('applicant')->where('applicant_id', $id)
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('location', 'position.fk_location', '=', 'location.location_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->where('fk_status', 'Valide')
      ->first();
    } else {
      $applicant = DB::table('applicant')->where('applicant_id', $id)
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('location', 'position.fk_location', '=', 'location.location_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->first();
    }
    // get responsibles
    $main_resp = DB::table('applicant')->where('applicant_id', $id)
      ->select('responsible_id', 'responsible_gender', 'responsible_name', 'responsible_fsname', 'responsible_street', 'responsible_npa', 'responsible_phone')
      ->join('responsible', 'applicant.fk_applicant_main_responsible', '=', 'responsible.responsible_id')
      ->first();

    $sec_resp = DB::table('applicant')->where('applicant_id', $id)
      ->select('responsible_id', 'responsible_gender', 'responsible_name', 'responsible_fsname', 'responsible_street', 'responsible_npa', 'responsible_phone')
      ->join('responsible', 'applicant.fk_applicant_sec_responsible', '=', 'responsible.responsible_id')
      ->first();

    // get scolarity
    $scolarities = DB::table('scolarity')->where('fk_applicant_id', $id)->get();

    // get pro activities
    $pro_activities = DB::table('professional_activity')->where('fk_applicant_id', $id)->get();

    // get trainings
    $trainings = DB::table('training')->where('fk_applicant_id', $id)->get();

    // get files (infos)
    $files = DB::table('file')->select('file_id', 'file_name')->where('fk_applicant_id', $id)->get();

    return [
      "personal_data" => $applicant,
      "responsibles" => [
        "main" => $main_resp,
        "secondary" => $sec_resp
      ],
      "scolarities" => $scolarities,
      "pro_activities" => $pro_activities,
      "trainings" => $trainings,
      "files" => $files
    ];
  }

  private function getApplicantJob ($id) {
    return DB::table('applicant')
    ->join('position', 'applicant.fk_position', '=', 'position.position_id')
    ->join('job', 'position.fk_job', '=', 'job.job_id')
    ->where('applicant_id', $id)
    ->pluck('job_short_value')->first();
  }
}
