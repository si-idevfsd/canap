<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
  private $request;
  private $user_data;
  private $user_sciper;
  private $user_permissions;

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_data = $this->request->get('user_data');
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
    $this->user_role = $this->request->get('user_role');
  }

  public function getData()
  {
    return response()->json([
      "tequila" => $this->user_data,
      "role" => $this->user_role,
      "permissions" => $this->user_permissions
    ]);
  }

  public function getPermittedJobs()
  {
    $result = [];

    foreach (DB::table('job')->get() as $key => $value) {
      foreach ($this->user_permissions as $key => $perm) {
        if ($perm == $value->job_short_value) {
          array_push($result, ["job_short_value" => $value->job_short_value, "job_full_value" => $value->job_full_value]);
        }
      }
    }
    return ($result);
    // TODO: remove duplicates
  }

  public function getCommentedAndMarkedApplicantsByUser()
  {
    $commented_applicants = DB::table('applicant')
      ->join('comment', 'applicant.applicant_id', '=', 'comment.comment_id')
      ->where('comment_owner_sciper', $this->user_sciper)->get();

    $marker_applicants = DB::table('applicant')
      ->join('marker', 'applicant.applicant_id', '=', 'marker.marker_id')
      ->where('marker_owner_sciper', $this->user_sciper)->get();

    return ["commented" => $commented_applicants, "marked" => $marker_applicants];
  }

  public function getUserDataBySciper($sciper)
  {
    $ldapconn = ldap_connect("ldap.epfl.ch", 389);
    $search = ldap_search($ldapconn, "o=epfl,c=ch", "uniqueIdentifier=".$sciper, array("displayName", "mail"));
    $info = ldap_get_entries($ldapconn, $search);
    return response()->json([
      "displayname" => $info[0]['displayname'][0],
      "mail" => $info[0]['mail'][0]
    ]);
  }

  public function getLastConnection ()
  {
    // TODO: this dont work, must be auth
    $last_connection = DB::table('last_connection')->where('last_connection_sciper', $this->user_sciper)->first();
    $now = date("Y-m-d H:i:s");
    return $last_connection ? ["last_connection" => $last_connection] : ["last_connection" => $now];
  }

  public function setLastConnection ()
  {
    // TODO: this dont work, must be auth
    $last_connection = DB::table('last_connection')->where('last_connection_sciper', $this->user_sciper)->first();
    if ($last_connection) {
      // Get tmp -> set to date, actual date to tmp
      $new_date = date("Y-m-d H:i:s");
      DB::table('last_connection')->where('last_connection_id', $last_connection->last_connection_id)->update([
        'last_connection_date' => $last_connection->last_connection_tmp_date,
        'last_connection_tmp_date' => $new_date
      ]);
    } else {
      // insert
      $new_date = date("Y-m-d H:i:s");
      $inserted_id = DB::table('last_connection')->insert([
        "last_connection_sciper" => $this->user_sciper,
        "last_connection_date" => $new_date,
        'last_connection_tmp_date' => $new_date
      ]);
    }
  }
}
