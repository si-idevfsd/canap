<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\AccessLevelHelper;
use Illuminate\Support\Facades\Lang;

class StatusController extends Controller
{
  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
    $this->user_role = $this->request->get('user_role');
  }

  public function getAvailableStatus()
  {
    return DB::table('status')->select()->get();
  }

  public function updateApplicantStatus ($id)
  {
    $applicant_job = DB::table('applicant')
    ->join('position', 'applicant.fk_position', '=', 'position.position_id')
    ->join('job', 'position.fk_job', '=', 'job.job_id')
    ->where('applicant_id', $id)
    ->pluck('job_short_value')->first();

    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);
    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');
    if ($has_access && $has_permitted_role) {
      $this->validate($this->request, [
        'status' => 'required|string'
      ], [lang::get('validation.required')]);

      $new_status = $this->request->input('status');

      // Valide le status
      if (count(DB::table('status')->where('status_value', $new_status)->get())) {
        DB::table('applicant')->where('applicant_id', $id)->update(['fk_status' => $new_status]);
        return ["message" => lang::get('http.success.updated.status'), "id" => $id];
      } else {
        return response()->json(['error' => 404, 'message' => lang::get('http.notfound')], 404);
      }
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }
}