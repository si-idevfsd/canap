<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Helpers\AccessLevelHelper;
use Illuminate\Support\Facades\Lang;

class CommentsController extends Controller
{
  private $request;
  private $user_sciper;
  private $user_permissions;

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
  }

  public function getApplicantComments($id)
  {
    $applicant_job = DB::table('applicant')
    ->join('position', 'applicant.fk_position', '=', 'position.position_id')
    ->join('job', 'position.fk_job', '=', 'job.job_id')
    ->where('applicant_id', $id)
    ->pluck('job_short_value')->first();
    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);

    if ($has_access) {
      $public_comments = DB::table('comment')->where('fk_applicant_id', $id)->where('comment_is_private', 0)->get();
      $private_comments = DB::table('comment')->where('fk_applicant_id', $id)->where('comment_is_private', 1)->where('comment_owner_sciper', $this->user_sciper)->get();
      return ["public" => $public_comments, "private" => $private_comments];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function create()
  {
    $this->validate($this->request, [
      'content' => 'required|string',
      'is_private' => 'required|boolean',
      'applicant_id' => 'required|numeric'
    ], [lang::get('validation.required')]);

    $new_content = $this->request->input('content');
    $new_is_private = $this->request->input('is_private');
    $new_date = date("Y-m-d H:i:s");
    $new_applicant_id = $this->request->input('applicant_id');

    $applicant_job = DB::table('applicant')
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->where('applicant_id', $new_applicant_id)
      ->pluck('job_short_value')->first();

    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);

    if ($has_access) {
      $inserted_id = DB::table('comment')->insertGetId([
        "comment_owner_sciper" => $this->user_sciper,
        "comment_content" => $new_content,
        "comment_is_private" => $new_is_private,
        "comment_date" => $new_date,
        "fk_applicant_id" => $new_applicant_id
      ]);
      return ["message" => lang::get('http.success.created.comment'), "id" => $inserted_id];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function delete($id)
  {
    $wanted_comment_exists = DB::table('comment')->where('comment_id', $id)->where('comment_owner_sciper', $this->user_sciper)->exists();
    if ($wanted_comment_exists) {
      DB::table('comment')->where('comment_id', $id)->where('comment_owner_sciper', $this->user_sciper)->delete();
      return ["message" => lang::get('http.success.deleted.comment')];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function update($id)
  {
    $this->validate($this->request, [
      'content' => 'required|string',
      'is_private' => 'required|boolean'
    ], [lang::get('validation.required')]);

    $new_content = $this->request->input('content');
    $new_is_private = $this->request->input('is_private');
    $new_date = date("Y-m-d H:i:s");

    $wanted_comment_exists = DB::table('comment')->where('comment_id', $id)->where('comment_owner_sciper', $this->user_sciper)->exists();
    if ($wanted_comment_exists) {
      DB::table('comment')->where('comment_id', $id)->where('comment_owner_sciper', $this->user_sciper)->update([
        'comment_content' => $new_content,
        'comment_is_private' => $new_is_private,
        'comment_date' => $new_date
      ]);
      return ["message" => lang::get('http.success.updated.comment'), "id" => $id];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

}
