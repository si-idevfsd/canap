<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\AccessLevelHelper;

class StatsController extends Controller
{
  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
  }

  public function getTotal()
  {
    return DB::table('applicant')
      ->select(DB::raw('job_short_value, job_full_value, count(*) as total'))
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->groupBy('job_short_value')
      ->get();
  }
}