<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Helpers\AccessLevelHelper;

class PositionsController extends Controller
{
  private $request;
  private $user_permissions;

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_permissions = $this->request->get('user_permissions');
    $this->user_role = $this->request->get('user_role');
  }

  public function getAll()
  {
    $results = [];
    $positions = DB::table('position')
      ->join('location', 'position.fk_location', '=', 'location.location_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->get();

    foreach ($positions as $position) {
      $position->access_groups = DB::table('has_access')->select('has_access_id','fk_position','fk_access_group as access_group_value')->where('has_access.fk_position', '=', $position->position_id)->get();
      array_push($results, $position);
    }
    return $results;
  }

  public function getAvailableLocations()
  {
    return DB::table('location')->get();
  }

  public function getAvailableJobs()
  {
    return DB::table('job')->get();
  }

  public function getAvailableAccessGroups()
  {
    return DB::table('access_group')->get();
  }

  public function createPosition()
  {
    $this->validate($this->request, [
      'position_spot_number' => 'required|numeric',
      'location_id' => 'required|numeric',
      'job_id' => 'required|numeric'
    ], [lang::get('validation.required')]);

    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');

    $new_position_access_groups = $this->request->input('access_group_cleared');
    $new_position_access_groups_to_create = $this->request->input('new_groups');
    $new_position_spot_number = $this->request->input('position_spot_number');
    $new_location_id = $this->request->input('location_id');
    $new_job_id = $this->request->input('job_id');

    if ($has_permitted_role) {
      $inserted_id = DB::table('position')->insertGetId([
        "position_spot_number" => $new_position_spot_number,
        "fk_location" => $new_location_id,
        "fk_job" => $new_job_id
      ]);


      foreach ($new_position_access_groups as $group) {
        DB::table('has_access')->insert([
          "fk_access_group" => $group['access_group_value'],
          "fk_position" => $inserted_id
        ]);
      }

      foreach ($new_position_access_groups_to_create as $group) {
        DB::table('access_group')->insertGetId([
          "access_group_value" => $group
        ]);

        DB::table('has_access')->insert([
          "fk_access_group" => $group,
          "fk_position" => $inserted_id
        ]);
      }

      return ["message" => lang::get('http.success.created.position'), "id" => $inserted_id];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function updatePosition($id)
  {
    $this->validate($this->request, [
      'position_spot_number' => 'required|numeric',
      'location_id' => 'required|numeric',
      'job_id' => 'required|numeric'
    ], [lang::get('validation.required')]);

    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');

    $position_access_groups = $this->request->input('access_group_cleared');
    $position_access_groups_to_create = $this->request->input('new_groups');
    $position_spot_number = $this->request->input('position_spot_number');
    $location_id = $this->request->input('location_id');
    $job_id = $this->request->input('job_id');

    $wanted_position_exists = DB::table('position')->where('position_id', $id)->exists();

    if ($wanted_position_exists && $has_permitted_role) {
      DB::table('position')->where('position_id', $id)->update([
        "position_spot_number" => $position_spot_number,
        "fk_location" => $location_id,
        "fk_job" => $job_id
      ]);

      // delete existing
      DB::table('has_access')->where('fk_position', $id)->delete();

      // Adding groups
      foreach ($position_access_groups as $group) {
        DB::table('has_access')->insert([
          "fk_access_group" => $group['access_group_value'],
          "fk_position" => $id
        ]);
      }

      foreach ($position_access_groups_to_create as $group) {
        DB::table('access_group')->insertGetId([
          "access_group_value" => $group
        ]);
        DB::table('has_access')->insert([
          "fk_access_group" => $group,
          "fk_position" => $id
        ]);
      }

      return ["message" => lang::get('http.success.updated.position'), "id" => $id];
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function deletePosition($id) {
    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');
    $wanted_position_exists = DB::table('position')->where('position_id', $id)->exists();
    $wanted_position_in_use = DB::table('applicant')->where('fk_position', $id)->exists();

    if ($wanted_position_exists && $has_permitted_role) {
      if ($wanted_position_in_use) {
        return response()->json(['error' => 403, 'message' => lang::get('http.error.deleted.position')], 403);
      } else {
        DB::table('has_access')->where('fk_position', $id)->delete();
        DB::table('position')->where('position_id', $id)->delete();
        return ["message" => lang::get('http.success.deleted.position')];
      }
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function createLocation () {
    $this->validate($this->request, [
      'location_site' => 'required'
    ], [lang::get('validation.required')]);

    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');

    $new_location = $this->request->input('location_site');

    if ($has_permitted_role) {
      $locationExists = DB::table('location')->where('location_site', $new_location)->exists();

      if (!$locationExists) {
        $inserted_id = DB::table('location')->insertGetId([
          "location_site" => $new_location
        ]);
        return ["message" => lang::get('http.success.created.location'), "id" => $inserted_id];
      
        } else {
          return response()->json(['error' => 400, 'message' => lang::get('http.error.location_exists')], 400);
      }
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function createJob () {
    $this->validate($this->request, [
      'job_full_value' => 'required'
    ], [lang::get('validation.required')]);

    $has_permitted_role = AccessLevelHelper::hasPermittedRole($this->user_role, 'responsable');

    $new_value = $this->request->input('job_full_value');
    $new_short_value = preg_replace('/[0-9]+/', '', preg_replace('/[^A-Za-z0-9\-]/', '', preg_replace('/\s+/', '', $new_value)));

    if ($has_permitted_role) {
      $jobExists = DB::table('job')->where('job_full_value', $new_value)->exists();

      if (!$jobExists) {
        $inserted_id = DB::table('job')->insertGetId([
          "job_full_value" => $new_value,
          "job_short_value" => $new_short_value
        ]);
        return ["message" => lang::get('http.success.created.job'), "id" => $inserted_id];

        } else {
          return response()->json(['error' => 400, 'message' => lang::get('job_exists')], 400);
        }
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }
}
