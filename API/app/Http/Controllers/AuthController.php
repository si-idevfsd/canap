<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use App\Providers\TequilaClient;
use Firebase\JWT\JWT;
use App\Helpers\AccessLevelHelper;

class AuthController extends Controller
{
  private $request;
  private $oClient;

  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  protected function jwt($tequila_attributes)
  {

    // DEV droits responsable
    $sciperDevResponsables = getenv('SCIPERDEVRESPONSABLE');

    if ($sciperDevResponsables !== false ) {
        if (array_search($tequila_attributes["uniqueid"], explode(",", $sciperDevResponsables))!== false) {
            $payload["role"] = 'responsable';
            $tequila_attributes["group"] = $tequila_attributes["group"] . ',responsables-apprentis';
        }
    }
  
    $user_perms = AccessLevelHelper::getUserAccess($tequila_attributes["group"]);
    $payload = [
      'iss' => "canap-gest",
      "tequila_data" => [
        "firstname" => $tequila_attributes["firstname"],
        "name" => $tequila_attributes["name"],
        "group" => $tequila_attributes["group"],
        "user" => $tequila_attributes["user"],
        "sciper" => $tequila_attributes["uniqueid"]
      ],
      'permissions' => $user_perms["groups"],
      "role" => $user_perms["role"],
      'iat' => time(),
      'exp' => time() + 43200
    ];



    return JWT::encode($payload, env('JWT_SECRET'));
  }

  public function login()
  {

    $oClient = new TequilaClient("https://tequila.epfl.ch/cgi-bin/tequila/");
    $oClient->setParam(
      array(
        'urlacces'  => url("/api/auth/tequilareturn"),
        'service'   => "Canap-Gest",
        'language'  => "francais",
        'usecookie' => "off"
      )
    );
    $oClient->setRequested(
      array(
        'request' => "name,firstname,uniqueid,group",
        //'require'    => "group=canap-gest-users"
        'require' => env('TEQUILA_REQUIRE_PARAMS', 'group=canap-gest-users')
      )
    );


    if (!empty($_GET['key'])) {
      $attributs = $oClient->checkUser($_GET['key']);

      if (!$attributs) {
        exit("Unknown tequila error");
      }
    } else {
      $oClient->createRequest();
      header("Location: " . $oClient->getAuthenticationUrl());
      exit;
    }
    return response()->json($this->jwt($attributs));
  }

  public function tequilareturn()
  {
    // return redirect()->to("https://canap-gest-dev.local:8080/#/?key=" . $_GET["key"]);
    // return redirect()->to("https://canap-gest.epfl.ch/#/?key=" . $_GET["key"]); // PROD
    // return redirect()->to("http://localhost:8080/#/?key=" . $_GET["key"]); // DEV
    return redirect()->to(env('TEQUILA_RETURN_URL', 'https://canap-gest.epfl.ch/#/?key=') . $_GET["key"]);
  }

  public function logout()
  {
    // $this->oClient->Logout();
  }
}
