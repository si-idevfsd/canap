<?php

namespace App\Http\Controllers;

use Log;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Symfony\Component\Yaml\Yaml;
use Carbon\Carbon;

class MailsController extends Controller
{
  // @TODO: Change every email hard written here as env vars!
  protected $request;
  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_sciper = $this->request->get('user_sciper');
    $this->user_permissions = $this->request->get('user_permissions');
  }

  public function mailApplicant()
  {
    $data = $this->validateMailData($this->request->all());
    if (!$data['success']) {
        return response()->json([
            'success' => false,
            'message' => $data['message'],
            'timeout' => 1000000,
        ]);
    }
      $headers = $this->prepareHeaders();
      $emailData = $this->prepareEmailData($headers);
      
      $this->logEmailData($emailData);
      
      $response = $this->sendEmails($emailData);
      return response()->json($response);
    }

  private function validateMailData(array $data)
  {
    $requiredFields = ['recipients', 'subject', 'content'];
    $missingFields = [];
    foreach ($requiredFields as $field) {
        if (!isset($data[$field]) || empty($data[$field])) {
            $missingFields[] = $field;
        }
    }

    $invalidCC = [];
    if (!empty($data['cc'])) {
        $invalidCC = $this->validateEmails($data['cc']);
    }
    
    $invalidBCC = [];
    if (!empty($data['bcc'])) {
        $invalidBCC = $this->validateEmails($data['bcc']);
    }
    
    $errorMessage = '';
    if (!empty($missingFields)) {
      $translatedFields = [];
      foreach ($missingFields as $field) {
        switch ($field) {
          case 'recipients':
            $translatedFields[] = 'destinataires';
            break;
          case 'subject':
            $translatedFields[] = 'sujet';
            break;
          case 'content':
            $translatedFields[] = 'contenu';
            break;
          default:
            $translatedFields[] = $field;
        }
      }

      $errorMessage .= 'Les champs suivants sont requis : ' . implode(', ', $translatedFields);
      if (!empty($invalidCC) || !empty($invalidBCC)) {
          $errorMessage .= ', ';
      }
    }
    
    if (!empty($invalidCC)) {
      $errorMessage .= 'CC invalide(s) : ' . implode(', ', $invalidCC);
      if (!empty($invalidBCC)) {
        $errorMessage .= ', ';
      }
    }
    
    if (!empty($invalidBCC)) {
      $errorMessage .= 'BCC invalide(s) : ' . implode(', ', $invalidBCC);
    }
    
    if (!empty($errorMessage)) {
      return [
        'success' => false,
        'message' => $errorMessage
      ];
    }
    
    return [
      'success' => true,
      'recipients' => $data['recipients'],
      'subject' => $data['subject'],
      'content' => $data['content'],
      'jobs' => $data['jobs'] ?? []
    ];
  }

  private function validateEmails($emailsString)
  {
    $invalidEmails = [];
    $emails = explode(',', $emailsString);

    foreach ($emails as $email) {
        if (!filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
            $invalidEmails[] = trim($email);
        }
    }
    return $invalidEmails;
  }

  private function prepareHeaders()
  {
    $headers = [
      'From' => 'noreply+candidatureApprentis@epfl.ch',
      'Bcc' => 'formation.apprentis@epfl.ch',
      'Cc' => '',
      'Reply-To' => 'formation.apprentis@epfl.ch',
      'X-Mailer' => 'PHP/' . phpversion()
    ];

    if ('prod' !== getenv('ENVIRONMENT')) {
      $headers['Bcc'] = "canap-ops@groupes.epfl.ch";
    }

    return $headers;
  }

  private function prepareEmailData($headers)
  {
    $emailData = [];
    foreach ($this->request->input('recipients') as $key => $recipient) {
      $content = str_replace("[METIER]", $this->request->input('jobs')[$key], $this->request->input('content'));
      
      if ($this->request->has('cc')) {
        $headers['Cc'] = $this->filterValidEmails($this->request->input('cc'));
      }
      
      if ($this->request->has('bcc')) {
        $headers['Bcc'] = $this->filterValidEmails($this->request->input('bcc'));
      }
      
      // Redefine the Bcc and Cc mail when not in prod
      $subject = $this->request->input('subject');
      if ('prod' !== getenv('ENVIRONMENT')) {
        $content = "RECIPIENT OF THIS MAIL HAS BEEN REWRITTEN\nOriginal recipient was: " . $recipient . "\n\n" . $content;
        $recipient = "canap-ops@groupes.epfl.ch";
        // Prefix the mail subject with the environment value when we are not in PROD!
        $subjectPrefix = "[" . getenv('ENVIRONMENT') . "] ";
        $subject = $subjectPrefix . $subject;
        
        // Rewriting CC and BCC when not in prod
        if ($this->request->has('cc')) {
          $headers['Cc'] = "canap-ops@groupes.epfl.ch";
        }

        if ($this->request->has('bcc')) {
          $headers['Bcc'] = "canap-ops@groupes.epfl.ch";
        }
      }

      $emailData[] = [
        'recipient' => $recipient,
        'bcc' => isset($headers['Bcc']) ? $headers['Bcc'] : '',
        'cc' => isset($headers['Cc']) ? $headers['Cc'] : '',
        'subject' => $subject,
        'content' => $content,
      ];
    }

    return $emailData;
  }

  private function filterValidEmails($emailsString)
  {
    $emails = explode(',', $emailsString);
    $validEmails = [];

    foreach ($emails as $email) {
      if (filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
        $validEmails[] = trim($email);
      }
    }

    return implode(', ', $validEmails);
  }

  private function sendEmails($emailData)
  {
      $headers = $this->prepareHeaders();
      $failedEmails = [];
      foreach ($emailData as $email) {
          if (!mail($email['recipient'], $this->request->input('subject'), $email['content'], $headers)) {
              $failedEmails[] = $email['recipient'];
          }
      }
      if (!empty($failedEmails)) {
        return [
          'success' => false,
          'message' => 'Échec de l\'envoi des emails à : ' . implode(', ', $failedEmails)
        ];
      }

      return [
          'success' => true,
          'message' => 'Les emails ont été envoyés avec succès'
      ];
  }

  private function logEmailData($emailData)
  {
    $formattedEmailData = [];

    foreach ($emailData as $email) {
        $formattedEmail = [
          'recipient' => $email['recipient'],
          'bcc' => $this->request->input('bcc') ?? '',
          'cc' => $this->request->input('cc') ?? '',
          'content' => $email['content'],
          'timestamp' => Carbon::now()->toIso8601String(),
        ];

      $formattedEmailData[] = $formattedEmail;
    }

    $logFileName = Carbon::now()->toDateString() . '.yaml';
    $yamlData = Yaml::dump($formattedEmailData);
    File::append(storage_path('logs/' . $logFileName), $yamlData);
  }
}
