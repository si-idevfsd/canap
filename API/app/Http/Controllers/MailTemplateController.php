<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MailTemplateController extends Controller
{
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function showAll()
    {
        $templates = DB::table('email_template')->get();
        return response()->json($templates);
    }
    public function create()
    {
        $this->validate($this->request, [
            'title' => 'required|string|max:200',
            'subject' => 'required|string|max:200',
            'content' => 'required|string',
        ]);

        $title = $this->request->input('title');
        $subject = $this->request->input('subject');
        $content = $this->request->input('content');

        $inserted_id = DB::table('email_template')->insertGetId([
            "title" => $title,
            "subject" => $subject,
            "content" => $content
        ]);

        return ["message" => "Template email créé avec succès.", "id" => $inserted_id];
    }

    public function update($id)
    {

        $this->validate($this->request, [
            'title' => 'required|string|max:200',
            'subject' => 'required|string|max:200',
            'content' => 'required|string', 
        ]);
        $title = $this->request->input('title');
        $subject = $this->request->input('subject');
        $content = $this->request->input('content');

        DB::table('email_template')->where('template_id', $id)->update([
            "title" => $title,
            "subject" => $subject,
            "content" => $content
        ]);

        return ["message" => "Template email mis à jour avec succès.", "id" => $id];
    }

    public function delete($id)
    {
        DB::table('email_template')->where('template_id', $id)->delete();
        return ["message" => "Template email supprimé avec succès."];
    }
}
