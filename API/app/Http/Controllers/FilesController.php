<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Reponse;
use Illuminate\Support\Facades\DB;
use App\Helpers\AccessLevelHelper;
use Illuminate\Support\Facades\Lang;
use ZipArchive;

class FilesController extends Controller
{
  private $request;
  private $user_permissions;

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->user_permissions = $this->request->get('user_permissions');
  }

  public function getFile($id)
  {
    // Check access to file
    $applicant_job = DB::table('applicant')
      ->join('file', 'file.fk_applicant_id', '=', 'applicant.applicant_id')
      ->join('position', 'applicant.fk_position', '=', 'position.position_id')
      ->join('job', 'position.fk_job', '=', 'job.job_id')
      ->where('file_id', $id)->pluck('job_short_value')->first();

    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);
    if ($has_access) {
      $file = DB::table('file')->where('file_id', $id)->first();
      return response()->download($file->file_path);
    } else {
      return response()->json(['error' => 403, 'message' => lang::get('http.unauthorized')], 403);
    }
  }

  public function exportApplicantFiles($id)
  {
    $applicant_job = DB::table('applicant')
    ->join('file', 'file.fk_applicant_id', '=', 'applicant.applicant_id')
    ->join('position', 'applicant.fk_position', '=', 'position.position_id')
    ->join('job', 'position.fk_job', '=', 'job.job_id')
    ->where('applicant_id', $id)->pluck('job_short_value')->first();
    $has_access = AccessLevelHelper::hasAccessToJob($applicant_job, $this->user_permissions);
    if ($has_access) {
      $tmp_dir = sys_get_temp_dir();
      $zipFileName = $id.'export.zip';
      $zip = new ZipArchive;

      $applicant_files = DB::table('file')
      ->where('fk_applicant_id', $id)->get();

      if ($zip->open($tmp_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
        foreach ($applicant_files as $file) {
          $zip->addFile($file->file_path, $file->file_name);
        }
        $zip->close();
      }

      $export_path = $tmp_dir .'/'. $zipFileName;
      return response()->download($export_path, $id .'export-annexes.zip', ['Content-Type' => 'application/zip'])->deleteFileAfterSend(true);
    }
  }
}
