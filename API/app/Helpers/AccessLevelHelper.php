<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class AccessLevelHelper
{
  public static function getDefaultAccessGroups () {
    $access_groups = [];
    $positions = DB::table('position')->select()->join('has_access', 'position_id', '=', 'fk_position')->get();

    foreach ($positions as $position) {
      $jobs = DB::table('job')->select()->where('job_id', $position->fk_job)->get();
      foreach ($jobs as $job) {
        if (isset($access_groups[$job->job_short_value])) {
          array_push($access_groups[$job->job_short_value], $position->fk_access_group);
        } else {
          $access_groups[$job->job_short_value][0] = $position->fk_access_group;
        }
      }
    }
    return $access_groups;
  }

  public static function hasAccessToJob($job, $permissions)
  {
    return in_array($job, $permissions);
  }

  public static function isJobValid($job)
  {
    $default_access_groups = self::getDefaultAccessGroups();
    foreach ($default_access_groups as $access_job => $access) {
      if ($access_job == $job) {
        return true;
      }
    }
    return false;
  }

  public static function hasPermittedRole($user_role, $wanted_role)
  {
    return $user_role == $wanted_role;
  }

  public static function getUserAccess($user_groups)
  {
    $default_access_groups = self::getDefaultAccessGroups();
    $user_alloweds = [];
    $user_groups_array = explode(',', $user_groups);

    if (self::isResponsible($user_groups_array)) {
      $user_role = 'responsable';
      foreach ($default_access_groups as $group => $accesses) {
        array_push($user_alloweds, $group);
      }
    }
    else {
      $user_role = 'formateur';
      foreach ($default_access_groups as $group => $accesses) {
        foreach ($accesses as $access) {
          if ((in_array($access, $user_groups_array))) {
            array_push($user_alloweds, $group);
          }
        }
      }
    }
    return ["groups" => array_unique($user_alloweds), "role" => $user_role];
  }

  public static function isResponsible($user_groups)
  {
    // $responsible_default_group = 'canap-gest-users-dev';
    $responsible_default_group = 'responsables-apprentis';
    return in_array($responsible_default_group, $user_groups);
  }
}