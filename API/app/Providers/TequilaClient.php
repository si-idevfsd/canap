<?php
/**
 * Client implementation for Tequila opaque mode (http://tequila.epfl.ch)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @copyright 2006 Camptocamp SA
 * @author Alexandre Saunier
 *
 * @package Plugins
 * @version $Id: $
 */


/* -----------------------------------------------------------------------------
Modifications
27.06.2008 - Nicolas Dubois
	Fonction getCurrentUrl -> ajoute check https + query_string
30.05.2006 - Nicolas Dubois
	- Support PHP 4
	- Check ssl CA (certificat de l'Autorit� de Certification EPFL).
		epflca.pem must be in the same directory as tequila_opaque.php
	- Check required attributes
------------------------------------------------------------------------------*/
namespace App\Providers;

/**
 * @package Plugins
 */
class TequilaClient {

	// Const
	var $EPFL_CA          = "quovadis.pem";
    var $TEQUILA_CREATE   = 'createrequest';
    var $TEQUILA_VALIDATE = 'validatekey';
    var $TEQUILA_REDIRECT = 'requestauth';
    var $TEQUILA_CONFIG   = 'getconfig';

	// Class attributes
    var $tequilaUrl;
	var $epflca;
    var $key;
    var $request;
	var $require;

	// Constructor
	// $tequilaUrl = tequila server url
	// $epflca     = path to EPFL certification authority (format: PEM)
    function __construct($tequilaUrl) {
        if (!extension_loaded('curl')) {
            exit('Extension CURL is not loaded');
        }
        $this->tequilaUrl = $tequilaUrl;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function setRequested($requested) {
        $this->requested = $requested;
    }


    /**
     * Sends a CURL request to the Tequila server.
     * @param string type of request
     * @param array array of fields to send
     * @return mixed
     */
    function askTequila($type, $fields = array()) {
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//~ curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__)."/".$this->EPFL_CA);

        $url = $this->tequilaUrl;
        switch ($type) {
            case 'create':
                $url .= $this->TEQUILA_CREATE;
                break;

            case 'validate':
                $url .= $this->TEQUILA_VALIDATE;
                break;

            case 'config':
                $url .= $this->TEQUILA_CONFIG;
                break;

            default:
                exit("Invalid Tequila request: $type");
        }
        curl_setopt($ch, CURLOPT_URL, $url);

        if (is_array($fields) && count($fields)) {
            $pFields = array();
            foreach ($fields as $key => $val) {
                $pFields[] = sprintf('%s=%s', $key, $val);
            }
            $query = implode("\n", $pFields);
			//~ print $query; exit();
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        }
        
        $response = curl_exec($ch);
		if (curl_errno($ch)) {
			die ("cURL error: ".curl_error($ch)."\n");
		}
        
        // If connexion failed (HTTP code 200 <=> OK)
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != '200') {
            $response = false;
        }
        
        curl_close($ch);

        return $response;
    }

    /**
     * Returns current URL.
     * @return string
     */
    function getCurrentUrl() {
		$url = $_SERVER["HTTPS"] == "off" ? "http://" : "https://";
		$url .= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];//$_SERVER['PHP_SELF'];
		// if ($_SERVER["QUERY_STRING"]) {
		// 	$url .= "?".$_SERVER["QUERY_STRING"];
		// }
		return $url;
    }

    /**
     * Sends an authentication request to Tequila.
     */
    function createRequest() {
        $response = $this->askTequila('create', array_merge($this->requested, $this->param));
        $this->key = substr(trim($response), 4); // 4 = strlen('key=')
    }

    /**
     * Checks that user has correctly authenticated and retrieves its data.
     * @return mixed
     */
    function checkUser($key) {
        $fields = array('key' => $key);
        $response = $this->askTequila('validate', $fields);

        if (!$response) {
            return false;
        }

        $attributes = $this->parseAttributes($response);
        // Check required
		foreach ($this->requested as $requestedAttributes) {
			foreach (explode(",", $requestedAttributes) as $requestedAttribute) {
                if (strpos($requestedAttribute, 'group=') !== false) {
                    $requested_group = str_replace('group=', '', $requestedAttribute);
                    $user_groups = explode(',', $attributes['group']);
                    if (!in_array($requested_group, $user_groups)) {
                        exit("Pas d'accès");
                    }
                } else if (!array_key_exists($requestedAttribute, $attributes)) {
					exit("Tequila error: missing attribute $requestedAttribute");
				}
			}
		}
        return $attributes;
    }

    /**
     * Gets Tequila Server config info.
     * @return string
     */
    function getConfig() {
        return $this->askTequila('config');
    }

    /**
     * Reads Tequila response and isolates returned attributes.
     * @param string
     * @return array
     */
    function parseAttributes($response) {
        $result = array();
        $attributes = explode("\n", $response);
        foreach ($attributes as $attribute) {
            $attribute = trim($attribute);
            if (!$attribute) {
                continue;
            }
            list($key, $val) = explode('=', $attribute);
            $result[$key] = $val;
        }
        return $result;
    }

    /**
     * Returns the Tequila authentication form URL.
     * @return string
     */
    function getAuthenticationUrl() {
        return sprintf('%s%s?requestkey=%s',
                       $this->tequilaUrl,
                       $this->TEQUILA_REDIRECT,
                       $this->key);
    }
}

?>
