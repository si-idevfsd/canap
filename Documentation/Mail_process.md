# Mail process of canap

## Gest
```mermaid
sequenceDiagram
    participant Z as Gest
    participant B as API
    participant A as Responsible
    participant X as Applicant
    
    Z->>B: POST to canap.epfl.ch/api/mail/applicant  

    B->>X: Send mail to
    B->>A: Send mail to
```
---
```mermaid
graph LR
    A[Gest-API] -->B(Answer with the selected response)
    B-->|Send mail to| D[The applicant]
    B-->|Send mail to| E[formation.apprentis@epfl.ch in Bcc/CC ]
```

### Files
| Name | Location | Definition |
| -------- | -------- | -------- |
| `ApplicationsView.vue`  | `Gestion/src/views/ApplicationsView.vue`     | displays a list of job applications in a user interface -> call the `mailDialog.vue` |
| `mutations.js` -> `setMailDialogOpened (state, data)`  |`Gestion/src/store/modules/applications/mutations.js`    |Modify the state of `mail_dialog_opened in the Vuex store according to the user's actions in order to open the dialog box for sending e-mails.
| `mailDialog.vue`  |`Gestion/src/components/application/mailDialog.vue`    | Dialog box that allows the user to compose and send e-mails. |
| `actions.js` -> `sendMails (context, data)`  |`Gestion/src/store/modules/applications/actions.js`    | Vuex action that sends emails using Axios to make a POST request to the api |
| `MailsController.php` |`API/app/Http/Controllers/MailsController.php`    | Vuex action that sends emails using Axios to make a POST request to the api |


## FORM
```mermaid
sequenceDiagram
    participant Z as Browser
    participant B as Form
    participant A as Responsible
    participant X as Applicant
    

    Z->>B: POST to canap.epfl.ch/form.php

    B->>X: Send mail to
    B->>A: Send mail to
```
---
```mermaid
graph LR
    A[Form] -->B(Form completed)
    B-->|Send mail to| D[formation.apprentis@epfl.ch]
    B-->|Send mail to| E[The applicant]
```

### Files

| Name | Location | Definition |
| -------- | -------- | -------- |
| `config.php`    | `Formulaire/configs/config.php`     | Configuration of the application to suit the environment |
| `cible.php`    | `Formulaire/cible.php` | Manages the confirmation of the application |
| `helpers.php` -> `mailToResp($surname, $name, $job)`   | `Formulaire/helpers.php` |Sends an e-mail to the managers with information about a new candidate|
| `helpers.php` -> `mailToApprenti($to)`   | `Formulaire/helpers.php` |Sends an e-mail to the applicant to confirm their application.|
| `helpers.php` -> `createFilesFolder($rootpath, $applicantId)`  | `Formulaire/helpers.php` |Creates a folder for the files associated with an apprentice's application.|
