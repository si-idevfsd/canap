# Création utilisateur pour connexion API -> DB
USE canap_db;
CREATE USER 'canap-gest-user'@'localhost' IDENTIFIED BY '<password>';
GRANT SELECT, UPDATE, INSERT, DELETE ON * TO 'canap-gest-user'@'localhost';
