<?php require('configs/db.php'); ?>
<!doctype html>
<html lang="fr">
  <head>
    <?php include('templates/head.php') ?>
    <title>Postulation Apprentis | Accès Refusé</title>
  </head>
  <body>
    <?php include('templates/header.php');?>
    <main id="main" role="main" class="content container-grid">
      <h3 style="color:red">Accès Refusé</h3>
      <p>La période des postulations n'a pas démarré.</p>
      <button class="btn btn-primary" id="retourHome">Retour à l'accueil</button>
    </main>
  </body>
</html>