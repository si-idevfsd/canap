$(document).ready(function () {
    initButtonsAction();
    if (location.search == "?edit") {
        $("#all").show();
        initButtonsAction();
        initAddChildButtons();
        initAddRadioButtonEvent();
        initDateChecker();
        initDatepicker();
        checkRequired();
        toggleTableHeaders();
    }
    else if (location.pathname.split("/").slice(-1)[0] == "form.php") {
        initButtonsAction();
        initAddChildButtons();
        initAddRadioButtonEvent();
        initDateChecker();
        initDatepicker();
        checkRequired();
    }
    if (location.pathname.split("/").slice(-1)[0] == "viewpostulation.php") {
        initPostulationChange();
    }
});

function checkDate() {
    var birthdate = $("#birthApp").val();
    var birthdate_year = birthdate.split('/')[2];
    var current_year = new Date().getFullYear();
    var diff = current_year - birthdate_year;

    if (diff <= 12 || diff > 60) {
        $('#birthApp').addClass("is-invalid");
        document.getElementById('errorBirthdate').innerHTML = 'Date invalide';
    }
    else {
        $('#birthApp').removeClass("is-invalid");
        document.getElementById('errorBirthdate').innerHTML = '';
    }
}

function initDateChecker() {
    $("#birthApp").change(function () {
        checkDate();
    });

    $("#dejaCandAnneeInput").change(function () {
        checkYearDate("#dejaCandAnneeInput", "#dejaCandError", 0);
    });
    $("#anneeFin").change(function () {
        checkYearDate("#anneeFin", "#anneeFinError", 1);
    });
}

function addChildren(parentId, maxItems, childTemplate, addButtonId) {
    var i = $(parentId + ' > tbody').children().length;
    if (i < maxItems) {
        var fullTemplate = childTemplate.replace(/\{(.*?)\}/g, i);
        $(parentId).append(fullTemplate);
        if (i == maxItems - 1) {
            $(addButtonId).hide();
        }
    }
    if (location.search == "?edit") {
        toggleTableHeaders();
    }
}

function changeTitleFile(objFile) {
    var filename = $(objFile)[0].value.replace(/^.*[\\/]/, '');
    if (filename) {
        location.search == "?edit" ? $(objFile.parentElement)[0].children[4].innerHTML = filename : $(objFile.parentElement)[0].children[3].innerHTML = filename;
    } else {
        location.search == "?edit" ? $(objFile.parentElement)[0].children[4].innerHTML = 'Remplacer...' : $(objFile.parentElement)[0].children[3].innerHTML = 'Choisir...';
    }
    checkFileFormat($(objFile)[0]);
}

function checkFileFormat(obj) {
    var errorZone = location.search == "?edit" ? $(obj.parentElement)[0].children[6] : $(obj.parentElement)[0].children[5];
    var allowedExtensions = ['pdf', 'jpeg', 'png', 'jpg'];
    var fileExtension = obj.value.split('.').pop().toLowerCase();

    allowedExtensions.indexOf(fileExtension) != -1 || !obj.value ? errorZone.innerHTML = '' : errorZone.innerHTML = 'Format invalide';
}

function checkYearDate(toCheckValue, errorZone, coefficiantYear) {
    var inputYear = $(toCheckValue).val();
    var currentYear = (new Date).getFullYear() + coefficiantYear;

    if ((inputYear != parseInt(inputYear, 10)) || (currentYear < inputYear) || (inputYear.length != 4)) {
        $(errorZone).html('Date invalide');
    } else {
        $(errorZone).html("");
    }
}

function initButtonsAction() {
    $('#createAc').click(function () {
        window.open("https://guests.epfl.ch/", '_blank');
    });
    $('#connectB').click(function () {
        document.location.href = "form.php";
    });
    $('#returnIndexB').click(function () {
        document.location.href = "/index.php";
    });
    $('#viewPostB').click(function () {
        document.location.href = "viewpostulation.php";
    });
    $('#delPostB').click(function () {
        if (confirm("Voulez-vous vraiment supprimer cette postulation ? \rCette action est irréversible !")) {
            document.location.href = "viewpostulation.php?delete";
        }
    });
    $('#editPost').click(function () {
        document.location.href = "/form.php?edit";
    });
    $('#cancelEditBtn').click(function () {
        document.location.href = "viewpostulation.php";
    })
    $('#infoFilieres').click(function () {
        window.open("https://www.ict-berufsbildung.ch/fr/formation-professionnelle/formation-initiale-ict/", '_blank');
    });
    $('#infoMpt').click(function () {
        window.open("https://orientation.ch/dyn/show/3309", '_blank');
    });
    $('#retourHome').click(function () {
        document.location.href = "http://apprentis.epfl.ch/";
    });
    $('#retourFormulaire').click(function () {
        history.go(-1);
    });
    $('#retourFormulaireEdit').click(function () {
        document.location.href = "/form.php?edit";
    });
    $('#retourAccueil').click(function () {
        document.location.href = "/index.php";
    });
}

function initAddChildButtons() {
    $('#addSch').click(function () {
        var template = '<tr><td><input type="text" name="ecole{i}" id="ecole{i}" placeholder="Ecole" class="form-control"/></td><td><input type="text" name="lieuEcole{i}" id="lieuEcole{i}" placeholder="Lieu" class="form-control"/></td><td><input type="text" name="niveauEcole{i}" id="niveauEcole{i}" placeholder="Niveau" class="form-control"/></td><td><input type="text" name="anneesEcole{i}" id="name="anneesEcole{i}" placeholder="de-à (années)" class="form-control"/></td></tr>';
        addChildren('#scolaire', 6, template, '#addSch');
    });

    $('#addPro').click(function () {
        var template = '<tr><td><input type="text" class="form-control" name="employeurPro{i}" id="employeurPro{i}" placeholder="Employeur"/></td><td><input type="text" class="form-control" name="lieuPro{i}" id="lieuPro{i}" placeholder="Lieu"/></td><td><input type="text" class="form-control" name="activitePro{i}" id="activitePro{i}" placeholder="Activité"/></td><td><input type="text" class="form-control" name="anneesPro{i}" id="anneesPro{i}" placeholder="de-à (années)"/></td></tr>';
        addChildren('#activites', 4, template, '#addPro');
    });

    $('#addStage').click(function () {
        var template = '<tr><td><input type="text" class="form-control" name="activiteStage{i}" id="activiteStage{i}" placeholder="Métier"></td><td><input type="text" class="form-control" name="entrepriseStage{i}" id="entrepriseStage{i}" placeholder="Entreprise"></td></tr>';
        addChildren('#stages', 5, template, '#addStage');
    });

    $('#addInputFile').click(function () {
        var nextIndex = $('#newCertifZone > tbody').children().length + 1;
        if (nextIndex < 10) {
            $('#newCertifZone').append('<tr><td><label for="annexe' + nextIndex + '">Annexe supplémentaire</label><br>'
                + '<input type="file" name="annexe' + nextIndex + '" class="upload-input" id="annexe' + nextIndex + '" onchange="changeTitleFile(this)"/>'
                + '<label for="annexe' + nextIndex + '" class="btn btn-secondary btn-sm icon-right">Choisir...</label><br>'
                + '<small class="error" class="form-text text-muted"></small></td></tr>');
            if (nextIndex == 9) {
                $('#addInputFile').hide();
            }
        }
    });
}

function toggleTableHeaders() {
    if ($('#activites')[0].rows.length == 1) {
        $($('#activites')[0].rows[0]).hide();
    } else {
        $($('#activites')[0].rows[0]).show();
    }
    if ($('#stages')[0].rows.length == 1) {
        $($('#stages')[0].rows[0]).hide();
    } else {
        $($('#stages')[0].rows[0]).show();
    }
}

function setMajState() {
    $('#maj1')[0].checked ? $("#representants").show() : $("#representants").hide();
}

function setDejaCandState() {
    if ($('#dejaCand1')[0].checked) {
        $("#dejaCandAnnee").hide();
        $("#dejaCandAnneeInput").val("");
        $("#dejaCandError").html("");
    } else {
        $("#dejaCandAnnee").show();
    }
}

function setLieuState() {
    if ($('#lieuLausanne')[0].checked) {
        $("#jbLausanne").show(0);
        $("#jbSion").hide(0);
    }
    else if ($('#lieuSion')[0].checked) {
        $("#jbSion").show(0);
        $("#jbLausanne").hide(0);
    }
}

function initAddRadioButtonEvent() {
    $('#maj1').change(setMajState);
    $('#maj2').change(setMajState);
    setMajState();

    $("#dejaCand1").change(setDejaCandState);
    $("#dejaCand2").change(setDejaCandState);
    setDejaCandState();
}

function clearRepresentants() {
    for (i = 1; i <= 2; i++) {
        $("#genreRep" + i + " :nth-child(1)").prop('selected', true);
        $("#nameRep" + i).val("");
        $("#surnameRep" + i).val("");
        $("#adrRep" + i).val("");
        $("#NPARep" + i).val("");
        $("#telRep" + i).val("");
    }
}

function initDatepicker() {
    $.datepicker.setDefaults($.datepicker.regional["fr"]);
    $("#birthApp").datepicker({ minDate: '-60y', maxDate: '-13y', dateFormat: "dd/mm/yy" });
}

function showListJob(lieu) {
    if (lieu == 'Lausanne') {
        $("#jbSion").hide();
        $("#jbLausanne").show();
    }
    else if (lieu == 'Sion') {
        $("#jbSion").show();
        $("#jbLausanne").hide();
    }
}
function showPolyAndInfoDivs(selectedFormation) {
    if (selectedFormation != "menu") {
        $("#all").show();
        selectedFormation == "informaticien" ? $("#infoOnly").show() : $("#infoOnly").hide();
        selectedFormation == "informaticien" ? $("#griTest").show() : $("#griTest").hide();
        selectedFormation == "polyMecanicien" ? $("#polyOnly").show() : $("#polyOnly").hide();
    }
}

function initPostulationChange() {
    $('#selectPost').on('change', function () {
        document.forms['postSelector'].submit();
    });
}

function checkRequired() {
    $("form").submit(function (e) {
        let errsNumber = 0;

        $($(this).find("[data-required]")).each(function () {
            if ($(this).val() == '') {
                $(this).addClass('is-invalid');
                e.preventDefault();
                errsNumber++;
            } else {
                $(this).removeClass('is-invalid');
            }
        });
        // no error
        if (errsNumber) {
            alert('Certains champs requis n\'ont pas été remplis');
        }
        return errsNumber === 0;
    });
}