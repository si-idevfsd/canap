<?php require('configs/db.php'); ?>
<!doctype html>
<html lang="fr">
<head>
  <title>Postulation Apprentis | Accueil</title>
  <?php include('templates/head.php'); ?>
</head>
<body>
  <?php include('templates/header.php'); ?>
  <main id="main" role="main" class="content container-grid">
    <h3 class="mb-5">Candidature pour un apprentissage</h3>
    <h5 class="mb-3">Veuillez suivre les indications suivantes</h5>
    <article>
      <ul class="list-bullet">
        <li>
          Il est nécessaire de vous munir des
          <a aria-describedby="footnote-label" id="footnotes-ref">documents suivants</a>
          <ul class="mb-4">
            <li>Lettre de motivation</li>
            <li>Curriculum Vitae avec indication des références</li>
            <li>Copie des bulletins scolaires des 3-4 derniers semestres</li>
            <li>Copies des certificats, diplômes obtenus, attestations de stages</li>
            <li>Copie carte d'identité</li>
            <li class="mb-3">Photo passeport couleur</li>
            <li>Pour informaticien, le certificat d’aptitude <a href="https://www.gri.ch/test-daptitudes/" target="_blank">GRI</a> peut être joint au dossier</li>
            <li>Pour polymécanicien, le certificat d’aptitude <a href="https://gim.swiss/formation/" target="_blank">GIM-CH</a> peut être joint au dossier</li>
          </ul>
        </li>
          <li class="mb-4">Veuillez prévoir une vingtaine de minutes pour compléter le formulaire</li>
          <li class="mb-4"><a aria-describedby="footnote-label" id="footnotes-ref">Créez un compte en cliquant sur le bouton suivant</a></li>
        </ul>
        <div>
          <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="createAc">Créer un compte</button>
          <br>
        </div>
        <ul class="list-bullet">
          <li class="mb-4">Confirmez ensuite votre adresse via l’email reçu.</li>
          <li class="mb-4">Une fois votre compte validé, cliquez sur le bouton suivant</li>
        </ul>
      <div>
        <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-5" id="connectB">Se connecter</button>
      </div>
      <ul class="list-bullet">
        <li>Après avoir effectué votre postulation, vous pouvez la consulter ici</li>
      </ul>
      <div>
        <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="viewPostB">Consulter ma postulation</button>
      </div>
      <footer>
        <h2 class="sr-only" id="footnote-label">Footnotes</h2>
        <ol>
          <li id="formats">Au format PDF, PNG, JPEG ou JPG</li>
          <li id="compte">Remplissez la rubrique "organisation" par "postulation"</li>
        </ol>
      </footer>
    </article>
  </main>
  <?php include('templates/footer.php'); ?>
</body>
</html>