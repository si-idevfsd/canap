<?php
  require('configs/db.php');
  require_once("tequila/tequila.php");
  require_once('tequila/tequila_instance.inc'); // $oClient->SetWishedAttributes(array('user'));
?>
<!doctype html>
    <html lang="fr">
      <head>
      <title>Postulation Apprentis | Formulaire</title>
        <?php include('templates/head.php'); ?>
      </head>
    <body>
    <?php
      include('templates/header.php');
      require_once("helpers.php");
      if (isset($_GET['edit'])) {
        // Check si une postulation a été selectionnée
        if (!isset($_SESSION['current_post']['applicant_id'])) {
          header('Location: index.php');
        } else {
          $current_post = $_SESSION['current_post'];
        }
      }
    ?>
    <main id="main" role="main" class="content container-grid">
      <h3 class="mb-5">Candidature pour un apprentissage</h3>
      <?php echo isset($_GET['edit']) ? "<small class='mb-3'>Les champs notés d'un * doivent être obligatoirement remplis.</small>" : "" ?>

      <?php if (isset($_GET['edit'])) {?>
        <form method="post" action="cible.php?edit" enctype="multipart/form-data">
      <?php } else { ?>
        <form method="post" action="cible.php" enctype="multipart/form-data">
      <?php } ?>
        <h3>1. Apprentissage</h3>
        <!-- Choix formation -->
        <div class="form-group">
          <?php isset($_GET['edit']) ? include('templates/formParts/edit/jobselection.php') : include('templates/formParts/new/jobselection.php'); ?>
        </div>
        <!-- / Choix formation -->

        <!-- div all -->
        <div id="all" >
        <!-- style="display: none;" -->

          <!-- Infos filières informaticien & Maturité -->
            <div class="form-group">
              <?php isset($_GET['edit']) ? include('templates/formParts/edit/maturity.php') : include('templates/formParts/new/maturity.php'); ?>
            </div>
          <!-- / Infos filières informaticien & Maturité -->
          <hr>

          <!-- Infos perso -->
            <h3>2. Données</h3>
            <h5>2.1 Données personnelles</h5>
            <div class="form-group">
              <?php isset($_GET['edit']) ? include('templates/formParts/edit/personnal.php') : include('templates/formParts/new/personnal.php'); ?>
            </div>
          <!-- / Infos perso -->

          <!-- Représentants -->
            <h5>2.2 Représentants légaux</h5>
            <div class="form-group">
              <?php isset($_GET['edit']) ? include('templates/formParts/edit/responsibles.php') : include('templates/formParts/new/responsibles.php'); ?>
            </div>
          <!-- / Représentants -->
          <hr>

          <!-- Activités -->
            <h3>3. Activités</h3>
            <!-- Scolarité -->
              <h5>3.1 Scolarité</h5>
              <?php isset($_GET['edit']) ? include('templates/formParts/edit/scolarity.php') : include('templates/formParts/new/scolarity.php'); ?>
            <!-- / Scolarité -->
            <!-- Formations -->
              <h5>3.2 Activités professionnelles</h5>
              <small>Formations / apprentissages après la scolarité.</small>
              <?php isset($_GET['edit']) ? include('templates/formParts/edit/proactivities.php') : include('templates/formParts/new/proactivities.php'); ?>
            <!-- / Formations -->
            <!-- Stages -->
              <h5 class="mt-3">3.3 Stages</h5>
              <?php isset($_GET['edit']) ? include('templates/formParts/edit/trainings.php') : include('templates/formParts/new/trainings.php'); ?>
            <!-- / Stages -->
          <!-- / Activités -->
          <!-- Déjà candidat -->
            <?php isset($_GET['edit']) ? include('templates/formParts/edit/alreadyapplicant.php') : include('templates/formParts/new/alreadyapplicant.php'); ?>
          <!-- / Déjà candidat -->
          <hr>

          <!-- Annexes -->
          <h3>4. Annexes</h3>
          <?php isset($_GET['edit']) ? include('templates/formParts/edit/annexes.php') : include('templates/formParts/new/annexes.php'); ?>
          <!-- / Annexes -->

          <!-- Conditions -->
          <div class="form-group">
            <div class="custom-control custom-checkbox" id="condDiv">
              <input type="checkbox" class="custom-control-input" value="conditionsAcc" id="conditions" data-required/>
              <label class="custom-control-label" for="conditions">Accepter les <a href="conditions.php" target="_blank"> conditions*</a></label>
            </div>
          </div>
          <!-- / Conditions -->
          <button class="btn btn-primary btn-block" type="submit">Terminer</button>
          <?php if (isset($_GET['edit'])) { ?>
            <button class="btn btn-secondary btn-block" id="cancelEditBtn" type="button">Annuler</button>
          <?php } ?>
        </div>
        <!-- / div all -->
      </form>
    </main>
    <?php include('templates/footer.php'); ?>
  </body>
</html>