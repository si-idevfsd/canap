<label>Je désire m'inscire en maturité professionnelle intégrée:</label>
<div class="custom-control custom-radio">
  <input type="radio" name="mpt" id="mpt1" class="custom-control-input" value="false" <?php echo ($current_post['applicant_maturity'] == 0) ? "checked=\"checked\"" : ''; ?>>
  <label class="custom-control-label" for="mpt1">Non</label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" name="mpt" id="mpt2" class="custom-control-input" value="true"  <?php echo ($current_post['applicant_maturity'] == 1) ? "checked=\"checked\"" : ''; ?>>
  <label class="custom-control-label" for="mpt2">Oui</label>
</div>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="infoMpt">Infos sur la maturité professionnelle</button>