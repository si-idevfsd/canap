<div class="form-group mt-3">
  <label>Avez-vous déjà été candidat à l'EPFL ?</label>
  <div class="custom-control custom-radio">
    <input type="radio" name="dejaCand" id="dejaCand1" class="custom-control-input" value="false" <?php echo $current_post['applicant_already_applicant'] == 0 ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="dejaCand1">Non</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="dejaCand" id="dejaCand2" class="custom-control-input" value="true" <?php echo $current_post['applicant_already_applicant'] == 1 ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="dejaCand2">Oui</label>
  </div>
</div>
<div class="form-group" id="dejaCandAnnee" style="display: none;">
  <label for="dejaCandAnneeInput">Année de candidature *</label>
  <input type="text" name="dejaCandAnnee" id="dejaCandAnneeInput" class="form-control" placeholder="Année de candidature" value="<?php echo $current_post['applicant_already_applicant_year']?>" maxlength="4"/>
  <small id="dejaCandError" class="form-text text-muted error"></small>
</div>