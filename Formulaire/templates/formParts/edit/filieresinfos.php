<div class="form-group">
<?= $current_post['applicant_it_section'] ?>
  <label>Filières informatiques:*</label>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill1" class="custom-control-input" value="developpementApplications" <?php echo ($current_post['applicant_it_section'] == "developpementApplications") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill1">Développement d'application</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill2" class="custom-control-input" value="entreprise" <?php echo ($current_post['applicant_it_section'] == "entreprise") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill2">Informatique d'entreprise</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill3" class="custom-control-input" value="techniqueSysteme" <?php echo ($current_post['applicant_it_section'] == "techniqueSysteme") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill3">Technique des systèmes</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill4" class="custom-control-input" value="neSaisPas" <?php echo ($current_post['applicant_it_section'] == '' || $current_post['applicant_it_section'] == "neSaisPas") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill4">Je ne sais pas</label>
  </div>
</div>
<button type="button" class="btn btn-primary btn-sm mb-3" id="infoFilieres">Infos sur les filières</button>