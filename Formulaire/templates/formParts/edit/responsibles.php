<?php
  if ($current_post['applicant_has_majority'] == 0) {
    $main_resp = getResponsibleById($pdo, $current_post['fk_applicant_main_responsible']);
    $sec_resp = getResponsibleById($pdo, $current_post['fk_applicant_sec_responsible']);
  
    if (!empty($main_resp)) {
      $main_resp = $main_resp[0];
    }
    if (!empty($sec_resp)) {
        $sec_resp = $sec_resp[0];
    }
  }
?>

<label>Avez vous plus de 18 ans ?*:</label>
<div class="custom-control custom-radio">
  <input type="radio" name="maj" id="maj1" class="custom-control-input" value="false" <?php echo $current_post['applicant_has_majority'] == 0 ? "checked=\"checked\"" : ''; ?>>
  <label class="custom-control-label" for="maj1">Non</label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" name="maj" id="maj2" class="custom-control-input" value="true" <?php echo $current_post['applicant_has_majority'] == 1 ? "checked=\"checked\"" : ''; ?>>
  <label class="custom-control-label" for="maj2">Oui</label>
</div>

<div class="form-group" id="representants">
  <p>Représentant principal:*</p>
  <input name="idRep1" value="<?= $main_resp['responsible_id'] ?? null ?>" hidden readonly>
  <label for="genreRep1">Genre *</label>
  <select name="genreRep1" id="genreRep1" class="custom-select">
    <option <?php echo (!isset($main_resp['responsible_gender'])) ? "selected" : ''; ?> disabled> Choisissez un genre</option>
    <option value="Homme" <?php echo ($main_resp['responsible_gender'] ?? null) == "Homme" ? "selected" : ''; ?>>Homme</option>
    <option value="Femme" <?php echo ($main_resp['responsible_gender'] ?? null) == "Femme" ? "selected" : ''; ?>>Femme</option>
  </select>
  <label for="nameRep1">Nom *</label>
  <input type="text" name="nameRep1" id="nameRep1" class="form-control" placeholder="Nom" value="<?php echo $main_resp['responsible_name'] ?? null; ?>" />
  <label for="surnameRep1">Prénom *</label>
  <input type="text" name="surnameRep1" id="surnameRep1" class="form-control" placeholder="Prénom" value="<?php echo $main_resp['responsible_fsname'] ?? null; ?>" />
  <label for="adrRep1">Rue *</label>
  <input type="text" name="adrRep1" id="adrRep1" class="form-control" placeholder="Rue" value="<?php echo $main_resp['responsible_street'] ?? null; ?>" />
  <label for="NPARep1">NPA, Domicile *</label>
  <input type="text" name="NPARep1" id="NPARep1" class="form-control" placeholder="NPA, Domicile" value="<?php echo $main_resp['responsible_npa'] ?? null; ?>" />
  <label for="telRep1">Téléphone *</label>
  <input type="text" name="telRep1" id="telRep1" class="form-control" placeholder="+41 79 123 45 67" value="<?php echo $main_resp['responsible_phone'] ?? null; ?>" />

  <p class="pt-4">Représentant secondaire:</p>
  <input name="idRep2" value="<?= $sec_resp['responsible_id'] ?? null ?>" readonly hidden>
  <label for="genreRep2">Genre</label>
  <select name="genreRep2" id="genreRep2" class="custom-select">
    <option <?php echo (!isset($sec_resp['responsible_gender'])) ? "selected" : ''; ?> disabled> Choisissez un genre</option>
    <option value="Homme" <?php echo ($sec_resp['responsible_gender'] ?? null) == "Homme" ? "selected" : ''; ?>>Homme</option>
    <option value="Femme" <?php echo ($sec_resp['responsible_gender'] ?? null) == "Femme" ? "selected" : ''; ?>>Femme</option>
  </select>
  <label for="nameRep2">Nom</label>
  <input type="text" name="nameRep2" id="nameRep2" class="form-control" placeholder="Nom" value="<?php echo $sec_resp['responsible_name'] ?? null; ?>" />
  <label for="surnameRep2">Prénom</label>
  <input type="text" name="surnameRep2" id="surnameRep2" class="form-control" placeholder="Prénom" value="<?php echo $sec_resp['responsible_fsname'] ?? null; ?>" />
  <label for="adrRep2">Rue</label>
  <input type="text" name="adrRep2" id="adrRep2" class="form-control" placeholder="Rue" value="<?php echo $sec_resp['responsible_street'] ?? null; ?>" />
  <label for="NPARep2">NPA, Domicile</label>
  <input type="text" name="NPARep2" id="NPARep2" class="form-control" placeholder="NPA, Domicile" value="<?php echo $sec_resp['responsible_npa'] ?? null; ?>" />
  <label for="telRep2">Téléphone</label>
  <input type="text" name="telRep2" id="telRep2" class="form-control" placeholder="+41 79 123 45 67" value="<?php echo $sec_resp['responsible_phone'] ?? null; ?>" />
</div>
