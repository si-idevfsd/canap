<?php
  $scolarites = getScolaritiesById($pdo, $current_post['applicant_id']);
?>
<table id="scolaire">
  <tr>
    <th>Ecole</th>
    <th>Lieu</th>
    <th>Niveau</th>
    <th>Années</th>
  </tr>
<?php foreach ($scolarites as $key => $scolarity) { $i = $key + 1; ?>
  <tr>
    <input type="text" name="ecole_id<?= $i ?>" value="<?= $scolarity['scolarity_id'] ?>" hidden readonly>
    <td>
      <input type="text" name="ecole<?= $i ?>" placeholder="Ecole *" class="form-control" value="<?php echo $scolarity['scolarity_school'] != '' ? $scolarity['scolarity_school'] : ''; ?>" data-required/>
    </td>
    <td>
      <input type="text" name="lieuEcole<?= $i ?>" placeholder="Lieu *" class="form-control" value="<?php echo $scolarity['scolarity_location'] != '' ? $scolarity['scolarity_location'] : ''; ?>" data-required/>
    </td>
    <td>
      <input type="text" name="niveauEcole<?= $i ?>" placeholder="Niveau *" class="form-control" value="<?php echo $scolarity['scolarity_level'] != '' ? $scolarity['scolarity_level'] : ''; ?>" data-required/>
    </td>
    <td>
      <input type="text" name="anneesEcole<?= $i ?>" placeholder="de-à (années) *" class="form-control" value="<?php echo $scolarity['scolarity_years'] != '' ? $scolarity['scolarity_years'] : ''; ?>" data-required/>
    </td>
  </tr>
  <?php } ?>
</table>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addSch">Ajouter une ligne</button>

<div class="form-group mt-3">
  <label for="anneeFin">Année de fin de scolarité*</label>
  <input type="text" name="anneeFin" id="anneeFin" class="form-control" placeholder="Année de fin de scolarité" value="<?php echo $current_post['applicant_scolarity_end'] != '' ? $current_post['applicant_scolarity_end'] : ''; ?>" maxlength="4" data-required/>
  <small id="anneeFinError" class="error form-text text-muted"></small>
</div>