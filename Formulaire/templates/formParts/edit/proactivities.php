<?php
  $proactivities = getProActivitiesById($pdo, $current_post['applicant_id']);
?>
<table id="activites">
  <tr>
    <th>Employeur</th>
    <th>Lieu</th>
    <th>Activité</th>
    <th>Années</th>
  </tr>
  <?php foreach ($proactivities as $key => $activity) { $i = $key + 1; ?>
  <tr>
  <input type="text" name="pro_id<?= $i ?>" value="<?= $activity['professional_activity_id'] ?>" hidden readonly>
    <td>
      <input type="text" name="employeurPro<?= $i ?>" placeholder="Employeur" class="form-control" value="<?php echo $activity['professional_activity_company'] != '' ? $activity['professional_activity_company'] : ''; ?>"/>
    </td>
    <td>
      <input type="text" name="lieuPro<?= $i ?>" placeholder="Lieu" class="form-control" value="<?php echo $activity['professional_activity_location'] != '' ? $activity['professional_activity_location'] : ''; ?>"/>
    </td>
    <td>
      <input type="text" name="activitePro<?= $i ?>" placeholder="Activité" class="form-control" value="<?php echo $activity['professional_activity_activity'] != '' ? $activity['professional_activity_activity'] : ''; ?>"/>
    </td>
    <td>
      <input type="text" name="anneesPro<?= $i ?>" placeholder="de-à (années)" class="form-control" value="<?php echo $activity['professional_activity_years'] != '' ? $activity['professional_activity_years'] : ''; ?>"/>
    </td>
  </tr>
  <?php } ?>
</table>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addPro">Ajouter une ligne</button>