<label for="genreApp">Genre</label>
<select name="genreApp" id="genreApp" class="custom-select" data-required>
  <option value="Homme" <?php echo ($current_post['applicant_gender'] == "Homme") ? "checked=\"checked\"" : ''; ?>>Homme</option>
  <option value="Femme" <?php echo ($current_post['applicant_gender'] == "Femme") ? "checked=\"checked\"" : ''; ?>>Femme</option>
</select>

<input type="text" name="guestSciper" class="form-control" value="<?php echo $guest_sciper;?>" readonly hidden />
<input type="text" name="id" class="form-control" value="<?php echo $_SESSION['current_post']['applicant_id'];?>" readonly hidden />

<label for="nameApp">Nom</label>
<input type="text" name="nameApp" id="nameApp" class="form-control" placeholder="Nom" value="<?php echo $current_post['applicant_name'] ?>" readonly />

<label for="surnameApp">Prénom</label>
<input type="text" name="surnameApp" id="surnameApp" class="form-control" placeholder="Prénom" value="<?php echo $current_post['applicant_fsname'] ?>" readonly />

<label for="adrApp">Rue</label>
<input type="text" name="adrApp" id="adrApp" placeholder="Rue" class="form-control" value="<?php echo $current_post['applicant_address_street'] ?>" minlength="2" maxlength="40" data-required/>

<label for="nameApp">NPA, Domicile</label>
<input type="text" name="NPAApp" id="NPAApp" placeholder="NPA, Domicile" class="form-control" value="<?php echo $current_post['applicant_address_npa'] ?>"  minlength="2" maxlength="40" data-required/>

<label for="telApp">Téléphone</label>
<input type="tel" name="telApp" id="telApp" placeholder="+41 21 123 45 67" class="form-control" value="<?php echo $current_post['applicant_home_phone'] ?>" minlength="2"  maxlength="20" data-required/>

<label for="telApp">Mobile</label>
<input type="tel" name="phoneApp" id="phoneApp" placeholder="+41 79 123 45 67" class="form-control" value="<?php echo $current_post['applicant_personal_phone'] ?>" minlength="2" maxlength="20" data-required/>

<label for="mailApp">Email</label>
<input type="email" name="mailApp" id="mailApp" value="<?php echo $current_post['applicant_mail'];?>" class="form-control" readonly />

<label for="birthApp">Date de naissance</label>
<input type="text" autocomplete="none" placeholder="jj/mm/aaaa" name="birthApp" id="birthApp" class="form-control" value="<?php echo $current_post['applicant_birthdate'] ?>" data-required />
<small id="errorBirthdate" class="error" class="form-text text-muted"></small>

<label for="originApp">Lieu d'origine</label>
<input type="text" name="originApp" id="originApp" placeholder="Lieu d'origine" class="form-control" value="<?php echo $current_post['applicant_origin'] ?>" minlength="2" maxlength="35" data-required />

<label for="nationApp">Nationalité</label>
<input type="text" name="nationApp" id="nationApp" placeholder="Nationalité" class="form-control" value="<?php echo $current_post['applicant_nationality'] ?>" minlength="2" maxlength="30" data-required />

<label for="permisEtrangerApp">Catégorie de permis pour étrangers</label>
<input type="text" name="permisEtrangerApp" id="permisEtrangerApp" class="form-control" placeholder="Catégorie de permis pour étrangers" value="<?php echo $current_post['applicant_foreign_authorization'] ?>" maxlength="1" />

<label for="langApp">Langue maternelle</label>
<input type="text" name="langApp" id="langApp" placeholder="Langue maternelle" class="form-control" value="<?php echo $current_post['applicant_main_language'] ?>" minlength="2" maxlength="20" data-required />

<label for="avsNumber">Numéro AV</label>
<input type="text" name="avsNumber" id="avsNumber" placeholder="Numéro AVS" class="form-control" value="<?php echo $current_post['applicant_avs'] ?>" minlength="2" maxlength="20" data-required />

<div class="form-group">
  <label>Connaissances linguistiques</label>

  <div class="custom-control custom-checkbox">
    <input hidden readonly value="0" name="applicant_speaks_french">
    <input type="checkbox" class="custom-control-input" value="1" id="french" name="applicant_speaks_french" <?php echo $current_post['applicant_speaks_french'] == 1 ? 'checked="checked"' : ''; ?>>
    <label class="custom-control-label" for="french">Français</label>
  </div>
  <div class="custom-control custom-checkbox">
    <input hidden readonly value="0" name="applicant_speaks_german">
    <input type="checkbox" class="custom-control-input" value="1" id="german" name="applicant_speaks_german" <?php echo $current_post['applicant_speaks_german'] == 1 ? 'checked="checked"' : ''; ?>>
    <label class="custom-control-label" for="german">Allemand</label>
  </div>
  <div class="custom-control custom-checkbox">
    <input hidden readonly value="0" name="applicant_speaks_english">
    <input type="checkbox" class="custom-control-input" value="1" id="english" name="applicant_speaks_english" <?php echo $current_post['applicant_speaks_english'] == 1 ? 'checked="checked"' : ''; ?>>
    <label class="custom-control-label" for="english">Anglais</label>
  </div>
  <div class="custom-control custom-checkbox">
    <input hidden readonly value="0" name="applicant_speaks_other">
    <input type="checkbox" class="custom-control-input" value="1" id="other" name="applicant_speaks_other" <?php echo $current_post['applicant_speaks_other'] == 1 ? 'checked="checked"' : ''; ?>>
    <label class="custom-control-label" for="other">Autres</label>
  </div>
</div>