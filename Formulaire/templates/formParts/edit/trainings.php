<?php
  $trainings = getTrainingById($pdo, $current_post['applicant_id']);
?>
<table id="stages">
  <tr>
    <th>Métier</th>
    <th>Entreprise</th>
  </tr>
  <?php foreach ($trainings as $key => $training) { $i = $key + 1; ?>
  <tr>
    <input type="text" name="stage_id<?= $i ?>" value="<?= $training['training_id'] ?>" hidden readonly>
    <td>
      <input type="text" class="form-control" name="activiteStage<?php echo $i ?>" placeholder="Métier" value="<?= $training['training_job'] ?>" >
    </td>
    <td>
      <input type="text" class="form-control" name="entrepriseStage<?php echo $i ?>" placeholder="Entreprise" value="<?= $training['training_company'] ?>" >
    </td>
  </tr>
  <?php } ?>
</table>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addStage">Ajouter une ligne</button>