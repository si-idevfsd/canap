<?php
  $files = getFilesById($pdo, $current_post['applicant_id']);
?>
<small>Remplacez ou ajouter des annexes (le fichier actuel est conservé si il n'est pas remplacé)</small>
<br>
<small>Formats autorisés: <b>PDF, JPG, JPEG, PNG</b></small>
<div class="form-group mt-3" id="files">
    <?php foreach ($files as $key => $file) {
        $name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file['file_name']);
    ?>
        <div class="form-group">
            <input value="<?= $file['file_id'] ?>" name="files[<?= $name ?>]" readonly hidden />
            <label for="file<?= $file['file_id'] ?>">Remplacer: <?= $file['file_name'] ?></label>
            <input type="file" name="<?= $name ?>" class="upload-input" id="<?= $name ?>" onchange="changeTitleFile(this)" />
            <br>
            <label for="<?= $name ?>" class="btn btn-secondary btn-sm icon-right">
                Remplacer...
            </label>
            <br>
            <small class="error" class="form-text text-muted"></small>
        </div>
    <?php } ?>
</div>
