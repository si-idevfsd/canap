<label>Je suis intéressé par la formation de:</label>
<select name="job" class="custom-select" data-required>
  <option value="menu" selected disabled>Choisir une formation...</option>
  <?php foreach ($DBController->getOpenPositions() as $position) {
    if (isset($_SESSION['postedForm']) && ($_SESSION['postedForm']['job'] ?? null) == $position['position_id']) { ?>
      <option value='<?= $position['position_id'] ?>' selected='selected'><?= $position['job_full_value'] ?> (<?= $position['location_site'] ?>)</option>
  <?php } else { ?>
      <option value='<?= $position['position_id'] ?>'><?= $position['job_full_value'] ?> (<?= $position['location_site'] ?>)</option>
  <?php }
  } ?>
</select>
