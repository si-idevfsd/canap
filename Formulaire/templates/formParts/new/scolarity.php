<table id="scolaire">
    <tr>
        <th>Ecole</th>
        <th>Lieu</th>
        <th>Niveau</th>
        <th>Années</th>
    </tr>
    <tr>
        <td>
            <input type="text" name="ecole1" placeholder="Ecole *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['ecole1'] != '' ? $_SESSION['postedForm']['ecole1'] : ''; ?>" data-required />
        </td>
        <td>
            <input type="text" name="lieuEcole1" placeholder="Lieu *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['lieuEcole1'] != '' ? $_SESSION['postedForm']['lieuEcole1'] : ''; ?>" data-required />
        </td>
        <td>
            <input type="text" name="niveauEcole1" placeholder="Niveau *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['niveauEcole1'] != '' ? $_SESSION['postedForm']['niveauEcole1'] : ''; ?>" data-required />
        </td>
        <td>
            <input type="text" name="anneesEcole1" placeholder="de-à (années) *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['anneesEcole1'] != '' ? $_SESSION['postedForm']['anneesEcole1'] : ''; ?>" data-required />
        </td>
    </tr>
    <tr>
        <td><input type="text" name="ecole2" placeholder="Ecole *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['ecole2'] != '' ? $_SESSION['postedForm']['ecole2'] : ''; ?>" data-required /></td>
        <td><input type="text" name="lieuEcole2" placeholder="Lieu *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['lieuEcole2'] != '' ? $_SESSION['postedForm']['lieuEcole2'] : ''; ?>" data-required /></td>
        <td><input type="text" name="niveauEcole2" placeholder="Niveau *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['niveauEcole2'] != '' ? $_SESSION['postedForm']['niveauEcole2'] : ''; ?>" data-required /></td>
        <td><input type="text" name="anneesEcole2" placeholder="de-à (années) *" class="form-control" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['anneesEcole2'] != '' ? $_SESSION['postedForm']['anneesEcole2'] : ''; ?>" data-required /></td>
    </tr>
    <?php
    for ($i = 3; $i < 6; $i++) {
        if (isset($_SESSION['postedForm']) && ($_SESSION['postedForm']['ecole' . $i] ?? null)) {
    ?>
            <tr>
                <td><input type="text" class="form-control" name="ecole<?php echo $i ?>" placeholder="Ecole" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['ecole' . $i] ?>" /></td>
                <td><input type="text" class="form-control" name="lieuEcole<?php echo $i ?>" placeholder="Lieu" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['lieuEcole' . $i] ?>"></td>
                <td><input type="text" class="form-control" name="niveauEcole<?php echo $i ?>" placeholder="Niveau" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['niveauEcole' . $i] ?>"></td>
                <td><input type="text" class="form-control" name="anneesEcole<?php echo $i ?>" placeholder="de-à (années)" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['anneesEcole' . $i] ?>"></td>
            </tr>
    <?php }
    } ?>
</table>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addSch">Ajouter une ligne</button>

<div class="form-group mt-3">
    <label for="anneeFin">Année de fin de scolarité*</label>
    <input type="text" name="anneeFin" id="anneeFin" class="form-control" placeholder="Année de fin de scolarité" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['anneeFin'] != '' ? $_SESSION['postedForm']['anneeFin'] : ''; ?>" maxlength="4" data-required />
    <small id="anneeFinError" class="error form-text text-muted"></small>
</div>
