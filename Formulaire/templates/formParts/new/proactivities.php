<table id="activites">
  <tr>
    <th>Employeur</th>
    <th>Lieu</th>
    <th>Activité</th>
    <th>Années</th>
  </tr>
  <tr>
    <td>
      <input type="text" class="form-control" name="employeurPro1" placeholder="Employeur" value="<?php echo (isset($_SESSION['postedForm']['employeurPro1']) && $_SESSION['postedForm']['employeurPro1'] != '') ? $_SESSION['postedForm']['employeurPro1'] : ''; ?>"/>
    </td>
    <td>
      <input type="text" class="form-control" name="lieuPro1" placeholder="Lieu" value="<?php echo (isset($_SESSION['postedForm']['lieuPro1']) && $_SESSION['postedForm']['lieuPro1'] != '') ? $_SESSION['postedForm']['lieuPro1'] : ''; ?>"/>
    </td>
    <td>
      <input type="text" class="form-control" name="activitePro1" placeholder="Activité" value="<?php echo (isset($_SESSION['postedForm']['activitePro1']) && $_SESSION['postedForm']['activitePro1'] != '') ? $_SESSION['postedForm']['activitePro1'] : ''; ?>"/>
    </td>
    <td>
      <input type="text" class="form-control" name="anneesPro1" placeholder="de-à (années)" value="<?php echo (isset($_SESSION['postedForm']['anneesPro1']) && $_SESSION['postedForm']['anneesPro1'] != '') ? $_SESSION['postedForm']['anneesPro1'] : ''; ?>"/>
    </td>
  </tr>
  <?php
    for ($i = 2; $i < 4; $i++) {
      if (isset($_SESSION['postedForm']['employeurPro'.$i]) && $_SESSION['postedForm']['employeurPro'.$i]) {
  ?>
  <tr>
    <td><input type="text" class="form-control" name="employeurPro<?php echo $i ?>" placeholder="Employeur" value="<?php echo $_SESSION['postedForm']['employeurPro'.$i] ?>"/></td>
    <td><input type="text" class="form-control" name="lieuPro<?php echo $i ?>" placeholder="Lieu" value="<?php echo $_SESSION['postedForm']['lieuPro'.$i] ?>" ></td>
    <td><input type="text" class="form-control" name="activitePro<?php echo $i ?>" placeholder="Activité" value="<?php echo $_SESSION['postedForm']['activitePro'.$i] ?>"></td>
    <td><input type="text" class="form-control" name="anneesPro<?php echo $i ?>" placeholder="de-à (années)" value="<?php echo $_SESSION['postedForm']['anneesPro'.$i] ?>"></td>
  </tr>
  <?php }} ?>
</table>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addPro">Ajouter une ligne</button>