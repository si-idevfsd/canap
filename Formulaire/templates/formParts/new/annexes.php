<small>Merci de joindre tous les fichiers demandés, en respectant les formats (si les formats ne sont pas respectés, les fichiers ne seront pas pris en compte).</small>
<br>
<small>Formats autorisés: <b>PDF, JPG, JPEG, PNG</b></small>
<!-- Annexes obligatoires -->
<div class="form-group mt-3" id="files">
  <div class="form-group">
    <label for="photo-passeport">Photo passeport <strong>couleur:</strong> *</label>
    <input type="file" name="photo-passeport" class="upload-input" id="photo-passeport" onchange="changeTitleFile(this)" data-required/>
    <br>
    <label for="photo-passeport" class="btn btn-secondary btn-sm icon-right">
      Choisir...
    </label>
    <br>
    <small class="error form-text text-muted"></small>
  </div>

  <div class="form-group">
    <label for="carte-identite">Copie carte d'indentité / passeport <strong>recto et verso</strong>: *</label>
    <input type="file" name="carte-identite" class="upload-input" id="carte-identite" onchange="changeTitleFile(this)" data-required/>
    <br>
    <label for="carte-identite" class="btn btn-secondary btn-sm icon-right">
      Choisir...
    </label>
    <br>
    <small class="error form-text text-muted"></small>
  </div>

  <div class="form-group">
    <label for="curriculum-vitae">Curriculum Vitae: *</label>
    <input type="file" name="curriculum-vitae" class="upload-input" id="curriculum-vitae" onchange="changeTitleFile(this)" data-required/>
    <br>
    <label for="curriculum-vitae" class="btn btn-secondary btn-sm icon-right">
      Choisir...
    </label>
    <br>
    <small class="error form-text text-muted"></small>
  </div>

  <div class="form-group">
    <label for="lettre-motivation">Lettre de motivation: *</label>
    <input type="file" name="lettre-motivation" class="upload-input" id="lettre-motivation" onchange="changeTitleFile(this)" data-required/>
    <br>
    <label for="lettre-motivation" class="btn btn-secondary btn-sm icon-right">
      Choisir...
    </label>
    <br>
    <small class="error form-text text-muted"></small>
  </div>
  <!-- / Annexes obligatoires -->
  <!-- Annexes spécifiques -->
  <div class="form-group" id="polyOnly">
    <label for="certificat-gimch"><b>Si polymécanicien·ne</b>, attestation de tests d'aptitudes  <a href="https://gim.swiss/formation/" target="_blank">GIM-CH</a>:</label>
    <input type="file" name="certificat-gimch" class="upload-input" id="certificat-gimch" onchange="changeTitleFile(this)"/>
    <br>
    <label for="certificat-gimch" class="btn btn-secondary btn-sm icon-right">
      Choisir...
    </label>
    <br>
    <small class="error form-text text-muted"></small>
  </div>

  <div class="form-group" id="griTest">
    <label for="certificat-gri"><b>Si informaticien·ne</b>, attestation de tests d'aptitudes <a href="https://www.gri.ch/test-daptitudes/" target="_blank">GRI</a>:</label>
    <input type="file" name="certificat-gri" class="upload-input" id="certificat-gri" onchange="changeTitleFile(this)"/>
    <br>
    <label for="certificat-gri" class="btn btn-secondary btn-sm icon-right">
      Choisir...
    </label>
    <br>
    <small class="error form-text text-muted"></small>
  </div>
  <!-- / Annexes spécifiques -->
  <!-- Annexes supplémentaires -->
  <div class="form-group">
    <table id="newCertifZone">
      <tr>
        <td>
          <label for="annexe1">Certificats, diplômes et bulletins de notes des derniers 3-4 semestres:</label>
          <input type="file" name="annexe1" class="upload-input" id="annexe1" onchange="changeTitleFile(this)"/>
          <br>
          <label for="annexe1" class="btn btn-secondary btn-sm icon-right">
            Choisir...
          </label>
          <br>
          <small class="error form-text text-muted"></small>
        </td>
      </tr>
    </table>
  </div>
  <button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addInputFile">Ajouter une annexe</button>
  <!-- region/ Annexes supplémentaires -->
</div>
