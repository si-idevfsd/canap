<div class="form-group mt-3">
  <label>Avez-vous déjà été candidat à l'EPFL ?</label>
  <div class="custom-control custom-radio">
    <input type="radio" name="dejaCand" id="dejaCand1" class="custom-control-input" value="false" <?php echo (!isset($_SESSION['postedForm']['dejaCand']) || $_SESSION['postedForm']['dejaCand'] == "false") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="dejaCand1">Non</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="dejaCand" id="dejaCand2" class="custom-control-input" value="true" <?php echo (isset($_SESSION['postedForm']) && $_SESSION['postedForm']['dejaCand'] == "true") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="dejaCand2">Oui</label>
  </div>
</div>
<div class="form-group" id="dejaCandAnnee" style="display: none;">
  <label for="dejaCandAnneeInput">Année de candidature *</label>
  <input type="text" name="dejaCandAnnee" id="dejaCandAnneeInput" class="form-control" placeholder="Année de candidature" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['dejaCandAnnee'] != '' ? $_SESSION['postedForm']['dejaCandAnnee'] : ''; ?>" maxlength="4"/>
  <small id="dejaCandError" class="form-text text-muted error"></small>
</div>
