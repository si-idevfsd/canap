<label>Avez vous plus de 18 ans ?*:</label>
<div class="custom-control custom-radio">
  <input type="radio" name="maj" id="maj1" class="custom-control-input" value="false" <?php echo (!isset($_SESSION['postedForm']['maj']) || $_SESSION['postedForm']['maj'] == "false") ? "checked=\"checked\"" : ''; ?>>
  <label class="custom-control-label" for="maj1">Non</label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" name="maj" id="maj2" class="custom-control-input" value="true"  <?php echo (isset($_SESSION['postedForm']['maj']) && $_SESSION['postedForm']['maj'] == "true") ? "checked=\"checked\"" : ''; ?>>
  <label class="custom-control-label" for="maj2">Oui</label>
</div>
</div>
<div class="form-group" id="representants">
<p>Représentant principal:*</p>
<label for="genreRep1">Genre *</label>
<select name="genreRep1" id="genreRep1" class="custom-select">
  <option <?php echo (!isset($_SESSION['postedForm']['genreRep1'])) ? "selected" : ''; ?> disabled> Choisissez un genre</option>
  <option value="Homme" <?php echo (isset($_SESSION['postedForm']['genreReo1']) && $_SESSION['postedForm']['genreRep1'] == "Homme") ? "selected" : ''; ?>>Homme</option>
  <option value="Femme" <?php echo (isset($_SESSION['postedForm']['genreReo1']) && $_SESSION['postedForm']['genreRep1'] == "Femme") ? "selected" : ''; ?>>Femme</option>
</select>
<label for="nameRep1">Nom *</label>
<input type="text" name="nameRep1" id="nameRep1" class="form-control" placeholder="Nom" value="<?php echo (isset($_SESSION['postedForm']['nameRep1']) && $_SESSION['postedForm']['nameRep1'] != '') ? $_SESSION['postedForm']['nameRep1'] : ''; ?>"/>
<label for="surnameRep1">Prénom *</label>
<input type="text" name="surnameRep1" id="surnameRep1" class="form-control" placeholder="Prénom" value="<?php echo (isset($_SESSION['postedForm']['surnameRep1']) && $_SESSION['postedForm']['surnameRep1'] != '') ? $_SESSION['postedForm']['surnameRep1'] : ''; ?>"/>
<label for="adrRep1">Rue *</label>
<input type="text" name="adrRep1" id="adrRep1" class="form-control" placeholder="Rue" value="<?php echo (isset($_SESSION['postedForm']['adrRep1']) && $_SESSION['postedForm']['adrRep1'] != '') ? $_SESSION['postedForm']['adrRep1'] : ''; ?>"/>
<label for="NPARep1">NPA, Domicile *</label>
<input type="text" name="NPARep1" id="NPARep1" class="form-control" placeholder = "NPA, Domicile" value="<?php echo (isset($_SESSION['postedForm']['NPARep1']) && $_SESSION['postedForm']['NPARep1'] != '') ? $_SESSION['postedForm']['NPARep1'] : ''; ?>"/>
<label for="telRep1">Téléphone *</label>
<input type="text" name="telRep1" id="telRep1" class="form-control" placeholder="+41 79 123 45 67" value="<?php echo (isset($_SESSION['postedForm']['telRep1']) && $_SESSION['postedForm']['telRep1'] != '') ? $_SESSION['postedForm']['telRep1'] : ''; ?>"/>

<p class="pt-4">Représentant secondaire:</p>
<label for="genreRep2">Genre</label>
<select name="genreRep2" id="genreRep2" class="custom-select">
  <option <?php echo (!isset($_SESSION['postedForm']['genreRep2'])) ? "selected" : ''; ?> disabled> Choisissez un genre</option>
  <option value="Homme" <?php echo (isset($_SESSION['postedForm']['genreRep2']) && $_SESSION['postedForm']['genreRep2'] == "Homme") ? "selected" : ''; ?>>Homme</option>
  <option value="Femme" <?php echo (isset($_SESSION['postedForm']['genreRep2']) && $_SESSION['postedForm']['genreRep2'] == "Femme") ? "selected" : ''; ?>>Femme</option>
</select>
<label for="nameRep2">Nom</label>
<input type="text" name="nameRep2" id="nameRep2" class="form-control" placeholder="Nom" value="<?php echo (isset($_SESSION['postedForm']['nameRep2']) && $_SESSION['postedForm']['nameRep2'] != '') ? $_SESSION['postedForm']['nameRep2'] : ''; ?>"/>
<label for="surnameRep2">Prénom</label>
<input type="text" name="surnameRep2" id="surnameRep2" class="form-control" placeholder="Prénom" value="<?php echo (isset($_SESSION['postedForm']['surnameRep2']) && $_SESSION['postedForm']['surnameRep2'] != '') ? $_SESSION['postedForm']['surnameRep2'] : ''; ?>"/>
<label for="adrRep2">Rue</label>
<input type="text" name="adrRep2" id="adrRep2" class="form-control" placeholder="Rue" value="<?php echo (isset($_SESSION['postedForm']['adrRep2']) && $_SESSION['postedForm']['adrRep2'] != '') ? $_SESSION['postedForm']['adrRep2'] : ''; ?>"/>
<label for="NPARep2">NPA, Domicile</label>
<input type="text" name="NPARep2" id="NPARep2" class="form-control" placeholder = "NPA, Domicile" value="<?php echo (isset($_SESSION['postedForm']['NPARep2']) && $_SESSION['postedForm']['NPARep2'] != '') ? $_SESSION['postedForm']['NPARep2'] : ''; ?>"/>
<label for="telRep2">Téléphone</label>
<input type="text" name="telRep2" id="telRep2" class="form-control" placeholder="+41 79 123 45 67" value="<?php echo (isset($_SESSION['postedForm']['telRep2']) && $_SESSION['postedForm']['telRep2'] != '') ? $_SESSION['postedForm']['telRep2'] : ''; ?>"/>