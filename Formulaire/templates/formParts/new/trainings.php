<table id="stages">
    <tr>
        <th>Métier</th>
        <th>Entreprise</th>
    </tr>
    <tr>
        <td>
            <input type="text" name="activiteStage1" class="form-control" placeholder="Métier" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['activiteStage1'] != '' ? $_SESSION['postedForm']['activiteStage1'] : ''; ?>">
        </td>
        <td>
            <input type="text" name="entrepriseStage1" class="form-control" placeholder="Entreprise" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['entrepriseStage1'] != '' ? $_SESSION['postedForm']['entrepriseStage1'] : ''; ?>">
        </td>
    </tr>
    <tr>
        <td><input type="text" name="activiteStage2" class="form-control" placeholder="Métier" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['activiteStage2'] != '' ? $_SESSION['postedForm']['activiteStage2'] : ''; ?>"></td>
        <td><input type="text" name="entrepriseStage2" class="form-control" placeholder="Entreprise" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['entrepriseStage2'] != '' ? $_SESSION['postedForm']['entrepriseStage2'] : ''; ?>"></td>
    </tr>
    <?php
    for ($i = 2; $i < 4; $i++) {
        if (isset($_SESSION['postedForm']) && ($_SESSION['postedForm']['activiteStage' . $i] ?? null)) {
    ?>
            <tr>
                <td><input type="text" class="form-control" name="activiteStage<?php echo $i ?>" placeholder="Métier" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['activiteStage' . $i] ?>" /></td>
                <td><input type="text" class="form-control" name="entrepriseStage<?php echo $i ?>" placeholder="Entreprise" value="<?php echo isset($_SESSION['postedForm']) && $_SESSION['postedForm']['entrepriseStage' . $i] ?>"></td>
            </tr>
    <?php }
    } ?>
</table>
<button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="addStage">Ajouter une ligne</button>
