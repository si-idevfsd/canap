<main id="main" role="main" class="content container-grid">
  <p>Votre demande à bien été enregistrée, vous allez bientôt recevoir un e-mail confirmant votre postulation.</p>
  <p>Vous avez la possibilité de consulter votre postulation depuis la page d'accueil de ce formulaire.</p>
  <button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="retourHome">Terminer</button>
  <br>
  <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="viewPostB">Consulter ma postulation</button>
</main>