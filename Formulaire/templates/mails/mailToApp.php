<?php
require_once('./configs/config.php');

$subject = 'Votre candidature pour une place d\'apprentissage';

$message = file_get_contents('./templates/mails/apprenticeMailTemplate.html');

$headers = 'From: ' . $CONFIG['MAIL_FROM'] . "\r\n" .
  'Content-type: text/html; charset= utf8' . "\r\n" .
  'Reply-To: ' . $CONFIG['MAIL_REPLYTO'] . "\r\n" .
  'X-Mailer: PHP/' . phpversion();
?>