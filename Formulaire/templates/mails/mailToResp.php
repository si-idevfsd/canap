<?php
require_once('./configs/config.php');

$to = $CONFIG['MAIL_CONTACT'];
$subject = 'Nouvelle Candidature';

$message = "Candidat: ".$surname." ".$name."\r\n".
  "Profession: ".$job."\r\n".
  "Consulter la candidature sur: https://canap-gest.epfl.ch/";

$headers = 'From: ' . $CONFIG['MAIL_FROM'] . "\r\n" .
  'Reply-To: ' . $CONFIG['MAIL_REPLYTO'] . "\r\n" .
  'X-Mailer: PHP/' . phpversion();
?>
