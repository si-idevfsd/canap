<div class="form-group">
  <label>Filières informatiques:*</label>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill1" class="custom-control-input" value="developpementApplications" <?php echo ($_SESSION['postedForm']['filInfo'] == "developpementApplications") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill1">Développement d'application</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill2" class="custom-control-input" value="entreprise"  <?php echo ($_SESSION['postedForm']['filInfo'] == "entreprise") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill2">Informatique d'entreprise</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill3" class="custom-control-input" value="techniqueSysteme"  <?php echo ($_SESSION['postedForm']['filInfo'] == "techniqueSysteme") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill3">Technique des systèmes</label>
  </div>
  <div class="custom-control custom-radio">
    <input type="radio" name="filInfo" id="fill4" class="custom-control-input" value="neSaisPas"  <?php echo (!isset($_SESSION['postedForm']['filInfo']) || $_SESSION['postedForm']['mpt'] == "neSaisPas") ? "checked=\"checked\"" : ''; ?>>
    <label class="custom-control-label" for="fill4">Je ne sais pas</label>
  </div>
</div>
<button type="button" class="btn btn-primary btn-sm mb-3" id="infoFilieres">Infos sur les filières</button>