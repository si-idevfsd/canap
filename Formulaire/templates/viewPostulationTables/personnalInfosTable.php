<button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#collapse-infosperso" aria-expanded="false" aria-controls="collapse-infosperso">
  Informations personnelles
</button>

<div class="collapse collapse-item show collapse-item-desktop" id="collapse-infosperso">
  <table class="table table-boxed mb-5">
    <thead>
      <tr>
        <th>Informations personnelles</th>
        <th>Données</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Formation</td>
        <td><?= $postulation['job_full_value'] ?></td>
      </tr>
      <tr>
        <td>Lieu </td>
        <td><?= $postulation['location_site'] ?></td>
      </tr>
      <tr>
        <td>Maturité intégrée</td>
        <td><?= $postulation['applicant_maturity'] ? "&#10004" : "&#10008" ?></td>
      </tr>
      <tr>
        <td>Nom, Prénom</td>
        <td><?= $postulation['applicant_name'] . " " . $postulation['applicant_fsname'] ?></td>
      </tr>
      <tr>
        <td>Genre</td>
        <td><?= $postulation['applicant_gender'] ?></td>
      </tr>
      <tr>
        <td>Email</td>
        <td><?= $postulation['applicant_mail'] ?></td>
      </tr>
      <tr>
        <td>Adresse</td>
        <td><?= $postulation['applicant_address_street'] . ", " . $postulation['applicant_address_npa'] ?></td>
      </tr>
      <tr>
        <td>Téléphones</td>
        <td><?= $postulation['applicant_home_phone'] . ", " . $postulation['applicant_personal_phone'] ?></td>
      </tr>
      <tr>
        <td>Date de naissance</td>
        <td><?= $postulation['applicant_birthdate'] ?></td>
      </tr>
      <tr>
        <td>Origine</td>
        <td><?= $postulation['applicant_origin'] ?></td>
      </tr>
      <tr>
        <td>Nationalité</td>
        <td><?= $postulation['applicant_nationality'] ?></td>
      </tr>
      <tr>
        <td>Catégorie de permis pour étrangers</td>
        <td><?= $postulation['applicant_foreign_authorization'] ?></td>
      </tr>
      <tr>
        <td>AVS</td>
        <td><?= $postulation['applicant_avs'] ?></td>
      </tr>
      <tr>
        <td>Langue maternelle</td>
        <td><?= $postulation['applicant_main_language'] ?></td>
      </tr>
      <tr>
        <td>Connaissances linguistiques</td>
        <td>
          <?php
            if ($postulation['applicant_speaks_french']) {
              echo '<img src="/assets/img/french_logo.png" alt="french_logo" width="30px">';
            }
            if ($postulation['applicant_speaks_german']) {
              echo '<img src="/assets/img/german_logo.png" alt="german_logo" width="30px">';
            }
            if ($postulation['applicant_speaks_english']) {
              echo '<img src="/assets/img/english_logo.png" alt="english_logo" width="30px">';
            }
            if ($postulation['applicant_speaks_other']) {
              echo '<img src="/assets/img/others_logo.png" alt="others_logo" width="30px">';
            }
          ?>
        </td>
      </tr>
      <tr>
        <td>Majeur</td>
        <td><?= $postulation['applicant_has_majority'] ? "&#10004" : "&#10008" ?></td>
      </tr>
      <tr>
        <td>Fin de scolarité</td>
        <td><?= $postulation['applicant_scolarity_end'] ?></td>
      </tr>
      <tr>
        <td>Déja candidat ? (année)</td>
        <td><?= $postulation['applicant_already_applicant'] ? "&#10004 (" .$postulation['applicant_already_applicant_year'] . ")" : "&#10008" ?></td>
      </tr>
    </tbody>
  </table>
</div>