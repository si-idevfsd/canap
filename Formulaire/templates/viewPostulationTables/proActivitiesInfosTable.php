<?php
  $proactivities = getProActivitiesById($pdo, $postulation['applicant_id']);
?>
<button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#collapse-pro" aria-expanded="false" aria-controls="collapse-pro">
  Activités professionnelles
</button>
<div class="collapse collapse-item show collapse-item-desktop" id="collapse-pro">
  <?php if (count($proactivities) < 1) { ?>
    <p style="text-align: center;">Aucune donnée fournie</p>
  <?php } ?>
  <?php foreach ($proactivities as $key => $activity) { ?>
    <table class="table table-boxed mb-5">
      <thead>
        <tr>
          <th>Activité professionnelle <?= $key + 1 ?></th>
          <th>Données</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Employeur, lieu</td>
          <td>
            <?= $activity['professional_activity_company'] . ", " . $activity['professional_activity_location'] ?>
          </td>
        </tr>
        <tr>
          <td>Activité</td>
          <td>
            <?= $activity['professional_activity_activity'] ?>
          </td>
        </tr>
        <tr>
          <td>Années</td>
          <td>
            <?= $activity['professional_activity_years'] ?>
          </td>
        </tr>
      </tbody>
    </table>
  <?php } ?>
</div>