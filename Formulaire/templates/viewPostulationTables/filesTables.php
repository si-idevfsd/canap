<?php
  $files = getFilesById($pdo, $postulation['applicant_id']);
?>
<button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#collapse-files" aria-expanded="false" aria-controls="collapse-files">
  Annexes
</button>
<div class="collapse collapse-item show collapse-item-desktop" id="collapse-files">
  <table class="table table-boxed mb-5">
    <thead>
      <tr>
        <th>Fichier</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($files as $key => $file) { ?>
      <tr>
        <td>
          <a href="/getFile.php?id=<?= $file['file_id']?>"><?= $file['file_name'] ?></a>
        </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
