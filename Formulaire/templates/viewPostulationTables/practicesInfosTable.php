<?php
  $trainings = getTrainingById($pdo, $postulation['applicant_id']);
?>
<button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#collapse-practices" aria-expanded="false" aria-controls="collapse-practices">
  Stages
</button>
<div class="collapse collapse-item show collapse-item-desktop" id="collapse-practices">
  <?php if (count($trainings) < 1) { ?>
    <p style="text-align: center;">Aucune donnée fournie</p>
  <?php } ?>
  <?php foreach ($trainings as $key => $training) { ?>
    <table class="table table-boxed mb-5">
      <thead>
        <tr>
          <th>Stage <?= $key + 1; ?></th>
          <th>Données</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Métier</td>
          <td>
            <?= $training['training_job'] ?>
          </td>
        </tr>
        <tr>
          <td>Entreprise</td>
          <td>
            <?= $training['training_company'] ?>
          </td>
        </tr>
      </tbody>
    </table>
  <?php } ?>
</div>