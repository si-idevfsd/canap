<?php
  $main_resp = getResponsibleById($pdo, $postulation['fk_applicant_main_responsible'])[0] ?? null;
  $sec_resp = getResponsibleById($pdo, $postulation['fk_applicant_sec_responsible'])[0] ?? null;
?>
<button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#collapse-responsibles" aria-expanded="false" aria-controls="collapse-responsibles">
  Représentants
</button>
<div class="collapse collapse-item show collapse-item-desktop" id="collapse-responsibles">
  <?php if (!$main_resp) { ?>
    <p style="text-align: center;">Aucune donnée fournie</p>
  <?php } ?>
  <?php if ($main_resp) { ?>
    <table class="table table-boxed mb-5">
      <thead>
        <tr>
          <th>Représentant principal</th>
          <th>Données</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Nom, prénom</td>
          <td>
            <?= $main_resp['responsible_name'] ?? null . ", " . $main_resp['responsible_fsname'] ?? null ?>
          </td>
        </tr>
        <tr>
          <td>Genre</td>
          <td>
            <?= $main_resp['responsible_gender'] ?? null ?>
          </td>
        </tr>
        <tr>
          <td>Adresse</td>
          <td>
            <?= $main_resp['responsible_street'] . ", " . $main_resp['responsible_npa'] ?>
          </td>
        </tr>
        <tr>
          <td>Téléphone</td>
          <td>
            <?= $main_resp['responsible_phone'] ?>
          </td>
        </tr>
      </tbody>
    </table>
  <?php } ?>
  <?php if ($sec_resp) { ?>
    <table class="table table-boxed mb-5">
      <thead>
        <tr>
          <th>Représentant secondaire</th>
          <th>Données</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Nom, prénom</td>
          <td>
            <?= $sec_resp['responsible_name'] . ", " . $sec_resp['responsible_fsname'] ?>
          </td>
        </tr>
        <tr>
          <td>Genre</td>
          <td>
            <?= $sec_resp['responsible_gender'] ?>
          </td>
        </tr>
        <tr>
          <td>Adresse</td>
          <td>
            <?= $sec_resp['responsible_street'] . ", " . $sec_resp['responsible_npa'] ?>
          </td>
        </tr>
        <tr>
          <td>Téléphone</td>
          <td>
            <?= $sec_resp['responsible_phone'] ?>
          </td>
        </tr>
      </tbody>
    </table>
  <?php } ?>
</div>
