<?php
  $scolarites = getScolaritiesById($pdo, $postulation['applicant_id']);
?>
<button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#collapse-scolarite" aria-expanded="false" aria-controls="collapse-scolarite">
  Scolarité
</button>
<div class="collapse collapse-item show collapse-item-desktop" id="collapse-scolarite">
  <?php if (count($scolarites) < 1) { ?>
    <p style="text-align: center;">Aucune donnée fournie</p>
  <?php } ?>
  <?php foreach ($scolarites as $key => $scolarity) { ?>
    <table class="table table-boxed mb-5">
      <thead>
        <tr>
          <th>Scolarité <?= $key + 1 ?></th>
          <th>Données</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Ecole, lieu</td>
          <td>
            <?= $scolarity['scolarity_school'] . ", " . $scolarity['scolarity_location'] ?>
          </td>
        </tr>
        <tr>
          <td>Niveau</td>
          <td>
            <?= $scolarity['scolarity_level'] ?>
          </td>
        </tr>
        <tr>
          <td>Années</td>
          <td>
            <?= $scolarity['scolarity_years'] ?>
          </td>
        </tr>
      </tbody>
    </table>
  <?php } ?>
</div>