<main id="main" role="main" class="content container-grid">
  <p>Vos modifications ont bien été enregistrées.</p>
  <p>Vous avez la possibilité de consulter votre postulation depuis la page d'accueil de ce formulaire.</p>
  <button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="retourHome">Terminer</button>
  <br>
  <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="viewPostB">Consulter ma postulation</button>
</main>