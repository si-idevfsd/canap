<header role="banner" class="header">
  <a class="logo">
    <img src="./assets/epfl2018/svg/epfl-logo.svg" alt="Logo EPFL, École polytechnique fédérale de Lausanne" class="img-fluid">
  </a>
  <ul aria-hidden="true" class="nav-header d-none d-xl-flex">
    <li>
      <a class="nav-item">Candidature pour un apprentissage</a>
    </li>
  </ul>
  <nav class="nav-lang nav-lang-short ml-auto">
    <a class="logo">
      <img src="./assets/img/FA.png" alt="Logo FA, Formation Apprentis EPFL" class="img-fluid">
    </a>
  </nav>
</header>