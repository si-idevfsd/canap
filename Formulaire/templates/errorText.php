<main id="main" role="main" class="content container-grid">
  <h5 style="color: red;">Des erreurs se sont produites, merci de remplir tous les champs obligatoires!</h5>
  <div id="formErrorsDiv" style="padding-bottom: 20px;">
    <?php
      if (!$_POST && !$_FILES) {
        echo "<li>Fichiers fournis trop volumineux ! <small>(Taille max. recommandée 2Mo)</small></li>";
      } else {
        foreach($validator->errors() as $error => $errorVal) {
          echo "<li>$errorVal</li>";
        }
      }
    ?>
  </div>
  <button type="button" class="btn btn-primary btn-sm mt-3 mb-2" id="retourFormulaire">Retour au formulaire</button>
</main>