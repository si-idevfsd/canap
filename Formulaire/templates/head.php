<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="./style/epfl2018/icons/favicon.png">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="./assets/epfl2018/css/vendors.min.css">
<link rel="stylesheet" href="./assets/epfl2018/css/base.css">
<link rel="icon" type="image/png" href="./assets/img/favicon.png" />
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="./assets/epfl2018/js/vendors.min.js"></script>
<script src="./assets/epfl2018/js/vendors.bundle.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="./script.js"></script>
<style>.error { color: red!important; }</style>
<noscript>Veuillez Activer JavaScript pour utiliser le service de postulation !</noscript>
<?php if (ENVIRONMENT === 'prod'): ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130041798-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-130041798-1', { 'anonymize_ip': true });
</script>
<?php endif; ?>