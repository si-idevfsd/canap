<?php
require_once('PersonnalData.php');
require_once(__DIR__ . '/../helpers.php');

class PersonnalDataValidator {
    #region [attributes]
    private $personnalData;
    private $errors = array();
    #endregion

    #region [contructor]
    public function __construct(PersonnalData $personnalData){
        $this->personnalData = $personnalData;
    }
    #endregion

    #region [validation methods]
    public function errors () {
        return $this->errors;
    }

    public function isValid() {
        $this->dataRequiredIsValid();
        $this->representantValid();
        $this->dejaCandValid();
        $this->isMailValid();
        $this->isEcoleValid();
        $this->anneeFinScolariteValid();
        $this->filesValid();

        return count($this->errors) === 0;
    }

    private function dataRequiredIsValid () {
        if (is_null($this->personnalData->genreApprenti) || $this->personnalData->genreApprenti == "" || $this->personnalData->genreApprenti =="notSelected") {
            $this->errors['genreApprenti'] = 'Genre non selectionné';
        }
        $toValid = array("nomApprenti" => $this->personnalData->nomApprenti,
        "prenomApprenti" => $this->personnalData->prenomApprenti,
        "formation" => $this->personnalData->formation,
        "addresseApprentiComplete" => $this->personnalData->addresseApprentiComplete,
        "telFixeApprenti" => $this->personnalData->telFixeApprenti,
        "telMobileApprenti" => $this->personnalData->telMobileApprenti,
        "mailApprenti" => $this->personnalData->mailApprenti,
        "dateNaissanceApprenti" => $this->personnalData->dateNaissanceApprenti,
        "origineApprenti" => $this->personnalData->origineApprenti,
        "nationaliteApprenti" => $this->personnalData->nationaliteApprenti,
        "langueMaternelleApprenti" => $this->personnalData->langueMaternelleApprenti,
        "numeroAVS" => $this->personnalData->numeroAVS);

        $this->isBirthDateValid(date($this->personnalData->dateNaissanceApprenti));

        foreach ($toValid as $key => $valid) {
            $this->isRequired($key, $valid);
        }
    }

    private function isBirthDateValid ($birthDate) {
        $birthYear = explode('/', $birthDate)[2];
        $actualYear = date('Y');

        if (($birthYear > $actualYear - 60) && ($birthYear <= $actualYear - 13)) {
        }
        else {
            $this->errors['dateNaissanceApprenti'] = 'Date de naissance non valide';
        }
    }

    private function isRequired ($key, $dataToCheck) {
        if (is_null($dataToCheck) || $dataToCheck == "") {
            $this->errors[$key] = $key . " manquant(e)";
        }
    }

    private function representantValid () {
        if (!$this->personnalData->majeur) {
            if (count($this->personnalData->representants) < 1 || $this->personnalData->representants[0]['nom'] == '') {
                $this->errors['representants'] = 'Représentants non valides';
            }
        }
    }
    private function dejaCandValid () {
        if ($this->personnalData->dejaCandidat) {
            if ($this->personnalData->anneeCandidature == "") {
                $this->errors['anneeCandidature'] = 'Année de candidature non valide';
            } else if (!is_numeric($this->personnalData->anneeCandidature)) {
                $this->errors['anneeCandidature'] = 'Année de candidature non valide';
            }
        }
    }
    private function isMailValid(){
        if (!filter_var($this->personnalData->mailApprenti, FILTER_VALIDATE_EMAIL)) {
            $this->errors['adresseMail'] = "Adresse mail non valide";
        }
    }
    private function isEcoleValid () {
        if (count($this->personnalData->scolarite) < 2) {
            $this->errors['ecole'] = 'Informations école non valides';
        }
    }
    private function anneeFinScolariteValid(){
        $dateToCheck = $this->personnalData->anneeFinScolarite;

        if(!is_null($dateToCheck) || $dateToCheck == ""){
            if(!is_numeric($dateToCheck)){
                $this->errors['anneeFinScolarite'] = 'Année de fin de scolarité non valide';
            }
        }
    }
    private function filesValid () {
        $filesToCheck = $this->personnalData->fichiers;
        $validExt = ['.pdf', '.jpeg', '.png', '.jpg'];
        $requiredKeys = ['photo-passeport', 'carte-identite', 'curriculum-vitae', 'lettre-motivation'];

        foreach($filesToCheck as $key => $file) {
            // Pour les fichiers obligatoire
            if (in_array($file['name'], $requiredKeys)) {
                // si fichier non fourni
                if (!$file['error'] == 0) {
                    $this->errors['fichiers'] = 'Fichier(s) non fourni(s)';
                }
            }

            // Check les formats
            if ($file['name'] != "") {
                $extension = strtolower(strrchr($file['name'], '.'));
                if(!in_array($extension, $validExt)){
                    $this->errors['fichiers'] = 'Format de fichier(s) non valable(s)';
                }
            }
        }
    }
    #endregion
}
?>