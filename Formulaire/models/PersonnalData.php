<?php
require_once(__DIR__ . '/../helpers.php');

class PersonnalData {
    #region [attributes]
    private $postedData;
    public $rootpath = '';
    public $id = "";
    public $guest_sciper = "";
    public $formation = "";
    public $maturite = 0;
    public $genreApprenti = "";
    public $nomApprenti = "";
    public $prenomApprenti  = "";
    public $addresseApprentiComplete = [];
    public $telFixeApprenti = "";
    public $telMobileApprenti = "";
    public $mailApprenti = "";
    public $dateNaissanceApprenti = "";
    public $origineApprenti = "";
    public $nationaliteApprenti = "";
    public $permisEtranger = "";
    public $numeroAVS = "";
    public $speaksFrench = 0;
    public $speaksGerman = 0;
    public $speaksEnglish = 0;
    public $speaksOther = 0;
    public $langueMaternelleApprenti = "";
    public $majeur = 0;
    public $representants = [];
    public $scolarite = [];
    public $anneeFinScolarite;
    public $activitesProfessionnelles = [];
    public $stages = [];
    public $dejaCandidat = 0;
    public $anneeCandidature = "";
    public $fichiers = [];
    #endregion

    #region [contructor]
    public function __construct($postedData, $postedFiles, $FILESERVERPATH) {
        $this->rootpath = $FILESERVERPATH;
        // Rempli les infos
        $this->postedData = $postedData;
        $this->id = isset($postedData['id']) ? $postedData['id'] : '';
        $this->guest_sciper = $postedData['guestSciper'];
        $this->formation = $postedData['job'];
        $this->maturite = $postedData['mpt'] == "true" ? 1 : 0;
        $this->genreApprenti = $postedData['genreApp'];
        $this->nomApprenti = $postedData['nameApp'];
        $this->prenomApprenti = $postedData['surnameApp'];
        $this->addresseApprentiComplete = array("rue" => $postedData['adrApp'], "NPA" => $postedData['NPAApp']);
        $this->telFixeApprenti = $postedData['telApp'];
        $this->telMobileApprenti = $postedData['phoneApp'];
        $this->mailApprenti = $postedData['mailApp'];
        $this->dateNaissanceApprenti = $postedData['birthApp'];
        $this->origineApprenti = $postedData['originApp'];
        $this->nationaliteApprenti = $postedData['nationApp'];
        $this->permisEtranger = $postedData['permisEtrangerApp'];
        $this->numeroAVS = $postedData['avsNumber'];
        $this->speaksFrench = $postedData['applicant_speaks_french'];
        $this->speaksGerman = $postedData['applicant_speaks_german'];
        $this->speaksEnglish = $postedData['applicant_speaks_english'];
        $this->speaksOther = $postedData['applicant_speaks_other'];
        $this->langueMaternelleApprenti = $postedData['langApp'];
        $this->majeur = $postedData['maj'] == "true" ? 1 : 0;
        if (!$this->majeur) { $this->setRepresentants(); }
        $this->setScolarite();
        $this->setActivitesPro();
        $this->setStages();
        $this->setDejacand();
        $this->anneeFinScolarite = $postedData['anneeFin'];
        $this->fichiers = $this->setFiles($postedFiles, $postedData['files'] ?? null);
    }
    #endregion

    #region [setters]
    private function setRepresentants()
    {
        $rep1 = array("genre" => $this->postedData['genreRep1'] ?? null, "nom" => $this->postedData['nameRep1'] ?? null, "prenom" => $this->postedData['surnameRep1'] ?? null, "addresse" => array("rue" => $this->postedData['adrRep1'] ?? null, "NPA" => $this->postedData['NPARep1'] ?? null), "telephone" => $this->postedData['telRep1'] ?? null);
        $rep2 = array("genre" => $this->postedData['genreRep2'] ?? null, "nom" => $this->postedData['nameRep2'] ?? null, "prenom" => $this->postedData['surnameRep2'] ?? null, "addresse" => array("rue" => $this->postedData['adrRep2'] ?? null, "NPA" => $this->postedData['NPARep2'] ?? null), "telephone" => $this->postedData['telRep2'] ?? null);
        if ($rep1) {
            if ($this->postedData['idRep1']) {
                $rep1['id'] = $this->postedData['idRep1'];
            }
            array_push($this->representants, $rep1);
        }
        if ($rep2) {
            if ($this->postedData['idRep2']) {
                $rep2['id'] = $this->postedData['idRep2'];
            }
            array_push($this->representants, $rep2);
        }
    }
    private function setScolarite () {
        for ($i = 1; $i <= 5; $i++) {
            if (array_key_exists('ecole' . $i, $this->postedData)) {
                if (isset($this->postedData['ecole_id' . $i])) {
                    array_push($this->scolarite, array("id" => $this->postedData['ecole_id' . $i], "ecole" => $this->postedData['ecole' . $i], "lieu" => $this->postedData['lieuEcole' . $i], "niveau" => $this->postedData['niveauEcole' . $i], "annees" => $this->postedData['anneesEcole' . $i]));
                } else {
                    array_push($this->scolarite, array("ecole" => $this->postedData['ecole' . $i], "lieu" => $this->postedData['lieuEcole' . $i], "niveau" => $this->postedData['niveauEcole' . $i], "annees" => $this->postedData['anneesEcole' . $i]));
                }
            }
        }
    }
    private function setActivitesPro () {
        for ($i = 1; $i <= 3; $i++) {
            if (array_key_exists('employeurPro' . $i, $this->postedData)) {
                if (isset($this->postedData['pro_id' . $i])) {
                    array_push($this->activitesProfessionnelles, array("id" => $this->postedData['pro_id' . $i], "employeur" => $this->postedData['employeurPro' . $i], "lieu" => $this->postedData['lieuPro' . $i], "activite" => $this->postedData['activitePro' . $i], "annees" => $this->postedData['anneesPro' . $i]));
                } else {
                    array_push($this->activitesProfessionnelles, array("employeur" => $this->postedData['employeurPro' . $i], "lieu" => $this->postedData['lieuPro' . $i], "activite" => $this->postedData['activitePro' . $i], "annees" => $this->postedData['anneesPro' . $i]));
                }
            }
        }
    }
    private function setStages () {
        for ($i = 1; $i <= 4; $i++) {
            if (array_key_exists('activiteStage' . $i, $this->postedData)) {
                if (isset($this->postedData['stage_id' . $i])) {
                    array_push($this->stages, array("id" => $this->postedData['stage_id' . $i], "metier" => $this->postedData['activiteStage' . $i], "employeur" => $this->postedData['entrepriseStage' . $i]));
                } else {
                    array_push($this->stages, array("metier" => $this->postedData['activiteStage' . $i], "employeur" => $this->postedData['entrepriseStage' . $i]));
                }
            }
        }
    }
    private function setDejacand () {
        $this->dejaCandidat = $this->postedData['dejaCand'] == "true";
        if ($this->dejaCandidat) {
            $this->anneeCandidature = $this->postedData['dejaCandAnnee'];
        }
    }
    private function setFiles ($postedFiles, $ids) {
        foreach ($postedFiles as $key => $file) {
            if ($file['error'] != 0) {
                unset($postedFiles[$key]);
            } else {
                $postedFiles[$key]['id'] = $ids[$key] ?? null;
            }
        }
        return $postedFiles;
    }
    #endregion
}
?>
