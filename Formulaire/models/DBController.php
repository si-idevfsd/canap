<?php

class DBController {
  private $pdo = '';
  public function __construct($pdo)
  {
    $this->pdo = $pdo;
  }

  public function getOpenPositions()
  {
    // Get position where number of spots > 0
    $sqlreq = "SELECT * FROM position JOIN location ON location_id = fk_location JOIN job ON job_id = fk_job WHERE position_spot_number > 0 ORDER by job_full_value ASC";
    $query = $this->pdo->prepare($sqlreq);
    $query->execute();
    return $query->fetchAll();
  }

  public function getPositionName(int $positionId)
  {
    $sqlreq = "SELECT * FROM position JOIN job ON job_id = fk_job JOIN location ON fk_location = location_id WHERE position_id = :position";
    $query = $this->pdo->prepare($sqlreq);
    $query->bindParam(':position', $positionId, PDO::PARAM_INT);
    $query->execute();
    $job = $query->fetch();
    return "{$job["job_full_value"]} ({$job["location_site"]})";
  }

}