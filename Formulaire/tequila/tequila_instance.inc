<?php
/**
 * Instanciation de Tequila
 **/

$baseurl="https://" . (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME']);

$oClient = new TequilaClient();
$oClient->SetApplicationName('Formulaire apprentissage');
$oClient->SetWantedAttributes(array('uniqueid','firstname','name', 'email'));
$oClient->SetWishedAttributes(array('user'));
$oClient->SetAllowsFilter('categorie=epfl-guests');
$oClient->SetApplicationURL($baseurl . "/" . $_SERVER['PHP_SELF']);
$oClient->Authenticate();
$user = $oClient->getValue('email');
$firstname = $oClient->getValue('firstname');
$name = $oClient->getValue('name');
$guest_sciper = $oClient->getValue('uniqueid');
$sKey = $oClient->GetKey();

