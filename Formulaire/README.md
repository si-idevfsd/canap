# Canap EPFL

Code source du site permettant de gerer les candidatures des futures apprentis
de l'EPFL. Canap = **CAN**didatures **AP**prentis.


## Mise en place

Avant toutes choses, copier le fichier
[config.template.php](./configs/config.template.php) vers
[config.php](./configs/config.php). Modifier les sections nécessaires pour
chaque environnement (`prod` → production, `local` → développement local, e.g.
WAMP, `docker` → pour développer avec [Docker](https://www.docker.com)).


## Améliorations en cours

- [x] Utiliser la nouvelle charte 2018
- [x] Adapter pour ajouter les postulations dans une DB
- [x] Ajouter une page pour voir / supprimer ses postulations
- [x] Commentaires et section du code
- [x] Editer ses postulations


## Améliorations possibles du formulaire canap.epfl.ch

- [ ] Refonte pour utiliser l'API (canap-gest)


## Infos

> Les candidatures s'ouvrent lorsque des places sont disponibles (admin. canap-gest)
> Résultats sur \\scxdata\apprentis$\candidatures et DB (canap_db)