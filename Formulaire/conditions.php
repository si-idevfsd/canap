<?php require('configs/db.php'); ?>
<!doctype html>
<html lang="fr">
  <head>
    <?php
      include('templates/head.php');
    ?>
    <title>Postulation Apprentis | Conditions</title>
  </head>
  <body>
    <?php include('templates/header.php') ?>
    <main id="main" role="main" class="content container-grid">
      <h3>Déclaration de protection des données</h3>

        <h4>Autorisation de traitement des postulations</h4>
        <p>
          En vous enregistrant auprès de l'EPFL, vous remettez à l'entreprise vos données personnelles pour une candidature en vue d'une recherche de place d'apprentissage
        </p>
        <p>
          Vos données seront enregistrées et traitées sur le système interne de la formation apprentis EPFL.
        </p>

        <h4>Vos droits</h4>
        <p>
          Vous avez à tout moment la possibilité d'accéder à vos données électroniques, de les modifier et de les supprimer en accédant à la page "<a href="http://canap-dev.epfl.ch/viewpostulation.php">Vos données</a>".
        </p>

        <h4>Utilisation de vos données personnelles pendant la procédure de candidature</h4>
        <p>
          Vos données sont enregistrées dans la base de données des candidats.
          Ces données sont enregistrées, évaluées, traitées et transmises en interne, exclusivement dans le cadre de votre candidature.
        </p>
        <p>
          Elles sont accessibles uniquement aux collaborateurs de la formation apprentis EPFL et aux formateurs recherchant des apprentis.
          Vos données ne sont en aucun cas transmises à des entreprises ou des personnes étrangères à l'EPFL ou utilisées pour d’autres finalités.
        </p>

        <h4>Conservation de vos données et suppression</h4>
        <p>
          Si votre candidature n'a pas été retenue, vos données sont supprimées après clôture de la campagne de candidature.
          Aucun avis de suppression des données n'est adressé.
        </p>
        <p>
          Vous pouvez à tout moment supprimer vos données en accédant à la page "<a href="http://canap-dev.epfl.ch/viewpostulation.php">Vos données</a>".
          Si vous intégrez l'EPFL, vos données personnelles sont portées à votre dossier personnel.
        </p>

      <h3>Renseignements</h3>
      <table>
        <tr>
          <td>Francis Perritaz</td>
        </tr>
        <tr>
          <td>Chef formation apprentis</td>
        </tr>
        <tr>
          <td>Téléphone : 021 693 31 19</td>
        </tr>
        <tr>
          <td><?php echo $CONFIG['MAIL_CONTACT']; ?></td>
        </tr>
        <tr>
          <td><a href="https://apprentis.epfl.ch" target="_blank">https://apprentis.epfl.ch</a></td>
        </tr>
      </table>
      <b class="mt-3">Tout dossier incomplet ou non conforme ne sera pas pris en considération ! </b>
    </main>
  </body>
</html>