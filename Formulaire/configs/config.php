<?php
    if (count(get_included_files()) == 1) exit("Direct access not permitted.");

    // Change with docker / local / prod
    define('ENVIRONMENT', getenv('ENVIRONMENT') ? getenv('ENVIRONMENT') : 'docker');


    $CONFIG = array(
        'FILESERVERPATH' => getenv('FILESERVERPATH'),
        'SENDEMAILS' => getenv('SENDEMAILS')=="yes",
        'EMAIL_FORMATION' => getenv('EMAIL_FORMATION'),
        'MAIL_FROM' => getenv('MAIL_FROM'),
        'MAIL_CONTACT' => getenv('MAIL_CONTACT'),
        'MAIL_REPLYTO' => getenv('MAIL_REPLYTO'),
        'DB_DATABASE' => getenv('DB_DATABASE'),
        'DB_USERNAME' => getenv('DB_USERNAME'),
        'DB_PASSWORD' => getenv('DB_PASSWORD'),
        'DB_HOST' => getenv('DB_HOST'),
        'DB_PORT' => getenv('DB_PORT'),
    );

    if (!isset($_SESSION)) { session_start(); }

    if (ENVIRONMENT !== 'prod') {
        ini_set("display_errors", 1);
        error_reporting(E_ALL);
    } else {
        ini_set("display_errors", 0);
        error_reporting(0);
    }
?>
