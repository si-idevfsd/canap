<?php

  require_once('configs/config.php');

  $pdo = new PDO('mysql:host=' . $CONFIG['DB_HOST'] . ';port=' . $CONFIG['DB_PORT'] . ';dbname=' . $CONFIG['DB_DATABASE'] . '', $CONFIG['DB_USERNAME'], $CONFIG['DB_PASSWORD'], array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  include('models/DBController.php');
  $DBController = new DBController($pdo);

  $POSTULATION_OPEN = count($DBController->getOpenPositions()) > 0; // close/open postulation

  if (!$POSTULATION_OPEN && $_SERVER['PHP_SELF'] != '/denied.php') { header("Location: ./denied.php"); }

?>
