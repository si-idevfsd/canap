<?php
  require('configs/db.php');
  include('./helpers.php');
  require_once("tequila/tequila.php");
  require_once('tequila/tequila_instance.inc');

  if (isset($_GET['id'])) {
    // check if sciper is linked to file
    $file = validFileAccess($pdo, $_GET['id'], $guest_sciper)[0];
    if ($file) {
      echo file_get_contents($file['file_path']);
      // send file
      header('Content-Disposition: attachment; filename='.$file['file_name']);
    } else {
      echo "ERREUR: Fichier inexistant ou accès refusé";
    }
  }
?>