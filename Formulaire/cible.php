<?php require('configs/db.php'); ?>
<!doctype html>
<html lang="fr">
  <head>
    <?php
      include("templates/head.php");
      require_once("helpers.php");
      require_once("models/PersonnalData.php");
      require_once("models/PersonnalDataValidator.php");
    ?>
    <title>Postulation Apprentis | Confirmation</title>
  </head>
  <body>
    <?php
      include('templates/header.php');

      // Init personnalData with postedData
      $candidateData = new PersonnalData($_POST, $_FILES, $CONFIG['FILESERVERPATH']);
      // Init dataValidator
      $validator = new PersonnalDataValidator($candidateData);

      if ($validator->isValid()) {
        if (isset($_GET['edit'])) {
          // update data
          updatePostulation($pdo, $candidateData);

          // get job name
          $job = $DBController->getPositionName($candidateData->formation);
          if ($CONFIG['SENDEMAILS']) {
            mailToResp($candidateData->prenomApprenti, $candidateData->nomApprenti, $job,$candidateData ->id, 'edit');
            mailToApprenti($candidateData->mailApprenti);
          }

          include("templates/confirmationText.php");
        } else {
          // write infos in DB
          $candidateId = insertDataIntoDB($pdo, $candidateData);

          // create dir + move files in it
          $filesPath = createFilesFolder($candidateData->rootpath, $candidateId);
          $candidateData = uploadAllFiles($filesPath, $candidateData->fichiers, $candidateData);

          // insert files in DB
          insertFiles($pdo, $candidateData->fichiers, $candidateId, $candidateData->rootpath);

          // get job name
          $job = $DBController->getPositionName($candidateData->formation);
          if ($CONFIG['SENDEMAILS']) {
            mailToResp($candidateData->prenomApprenti, $candidateData->nomApprenti, $job, $candidateId, 'new');
            mailToApprenti($candidateData->mailApprenti);
          }
          // kill session
          $_SESSION['formError'] = false;
          // unset($_SESSION);
          // $_SESSION = [];
          include("templates/confirmationText.php");
        }
      } else {
        $_SESSION['formError'] = true;
        if (isset($_GET['edit'])) {
          include("templates/errorTextEdit.php");
        } else {
          $_SESSION['postedForm'] = $_POST;
          $_SESSION['files'] = $_FILES;
          include("templates/errorText.php");
        }
      }
    ?>
  </body>
</html>