<?php
  require('configs/db.php');
  require_once("tequila/tequila.php");
  require_once('tequila/tequila_instance.inc');
  include('./helpers.php');
?>
<!doctype html>
<html lang="fr">
  <head>
    <title>Postulation Apprentis | Consultation</title>
    <?php
      include('templates/head.php');
    ?>
    <style>td {height: 40px;}</style>
  </head>
  <body>
    <?php include('templates/header.php'); $postulations = getPostulationBySciper($pdo, $guest_sciper);?>
    <main id="main" role="main" class="content container-grid">
      <h3 class="mb-5">Candidature pour un apprentissage</h3>
      <!-- si plusieures postulations -->
      <?php if (count($postulations) > 0) { ?>
        <?php if (count($postulations) > 1) { ?>
        <div class="form-group">
          <label>Vous avez effectué plusieurs postulations, merci de choisir</label>
          <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" name="postSelector">
            <select id="selectPost" name="selected_post" class="custom-select mb-3">
              <?php foreach ($postulations as $key => $postulation) { ?>
                <option <?= (isset($_POST['selected_post']) && $key == $_POST['selected_post']) ? 'selected' : '' ?> value="<?=$key?>"><?=$postulation['job_full_value'] . ", " . date('d/m/Y', strtotime($postulation['applicant_application_date'])) ?></option>
              <?php } ?>
            </select>
          </form>
          </div>
        <?php } ?>
        <article>
          <?php
            $_SESSION['edit_error'] = '';

            if (isset($_GET['delete'])) {
              deletePostulation($pdo, $_SESSION['current_post']['applicant_id'], $_SESSION['current_post']['applicant_guest_sciper'], $_SESSION['current_post']['fk_applicant_main_responsible'], $_SESSION['current_post']['fk_applicant_sec_responsible']);
            } else {
              if (isset($_POST['selected_post'])) {
                $postulation = $postulations[$_POST['selected_post']];
              } else {
                $postulation = $postulations[0];
              }
            }
            $_SESSION['current_post'] = $postulation;

            echo "<h4>Postulation du " . date('d/m/Y', strtotime($postulation['applicant_application_date'])) ;
            if ($postulation['applicant_application_updated_date']) {
              echo "<small style='float: right;'><i>Dernière modification le " . date('d/m/Y', strtotime($postulation['applicant_application_updated_date'])) ."</i></small>". "</h4>";
            }
            ?>
            <?php
              include('templates/viewPostulationTables/personnalInfosTable.php');
              include('templates/viewPostulationTables/responsiblesInfosTable.php');
              include('templates/viewPostulationTables/scolaritiesInfosTable.php');
              include('templates/viewPostulationTables/proActivitiesInfosTable.php');
              include('templates/viewPostulationTables/practicesInfosTable.php');
              include('templates/viewPostulationTables/filesTables.php');
            ?>
          <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="editPost">Modifier</button>
          <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="delPostB">Retirer ma postulation</button>
          <br>
          <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="retourAccueil">Retour</button>
          <?php } else { ?>
            <p>Aucune postulation effectuée.</p>
            <button type="button" class="btn btn-primary btn-block btn-sm mt-3 mb-2" id="returnIndexB">Retour</button>
          <?php } ?>
      </article>
    </main>
  </body>
</html>