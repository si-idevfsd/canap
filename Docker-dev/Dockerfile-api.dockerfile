FROM php:8.2-apache

RUN apt-get update; apt-get install -yq git vim zip libzip-dev msmtp msmtp-mta;
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN export PATH=$PATH:/root/.composer/vendor/bin

RUN docker-php-ext-install pdo pdo_mysql zip
RUN a2enmod rewrite

ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN echo "LimitRequestFieldSize 81920" > /etc/apache2/conf-enabled/say-yes-to-bearer-tokens.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN echo 'sendmail_path="/usr/bin/msmtp -t"' > /usr/local/etc/php/conf.d/mail.ini
# XDEBUG
RUN yes | pecl install xdebug
RUN docker-php-ext-enable xdebug

# Enable Remote xdebug
RUN echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini
RUN echo "xdebug.log=/proc/self/fd/1" >> /usr/local/etc/php/conf.d/xdebug.ini
RUN echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini
RUN echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/xdebug.ini
# https://github.com/docker/for-win/issues/12673 says host.docker.internal does not work (and we cannot trust the guessing mechanism, because
# we are going through Traefik):
RUN echo "xdebug.client_host=172.22.0.1" >> /usr/local/etc/php/conf.d/xdebug.ini