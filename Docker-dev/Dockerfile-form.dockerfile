FROM php:8.2-apache

RUN apt-get update;
RUN apt-get install -f -y msmtp msmtp-mta;

RUN echo 'sendmail_path = "/usr/bin/msmtp -C /etc/msmtprc -t"' >> /usr/local/etc/php/php.ini
RUN echo 'sendmail_from = "noreply@epfl.ch"' >> /usr/local/etc/php/php.ini
RUN touch /etc/msmtprc
RUN echo "defaults" > /etc/msmtprc
RUN echo "logfile        /var/log/msmtp.log" >> /etc/msmtprc
RUN echo "" >> /etc/msmtprc
RUN echo "account        epfl" >> /etc/msmtprc
RUN echo "host           mail.epfl.ch" >> /etc/msmtprc
RUN echo "port           25" >> /etc/msmtprc
RUN echo "from           noreply@epfl.ch" >> /etc/msmtprc
RUN echo "" >> /etc/msmtprc
RUN echo "account default : epfl" >> /etc/msmtprc


#php -r "mail('nicolas.borboen@epfl.ch', 'Test Postfix', 'Test mail from postfix');"

RUN docker-php-ext-install pdo_mysql

# cleanup
RUN apt-get -y autoremove && apt-get -y clean;

