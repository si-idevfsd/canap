-- --------------------------------------------------------
-- Hôte :                        mysql-fac.epfl.ch
-- Version du serveur:           5.7.21-log - MySQL Community Server (GPL)
-- SE du serveur:                linux-glibc2.12
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour canap_db
DROP DATABASE IF EXISTS `canap_db`;
CREATE DATABASE IF NOT EXISTS `canap_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `canap_db`;

-- Listage de la structure de la table canap_db. access_group
DROP TABLE IF EXISTS `access_group`;
CREATE TABLE IF NOT EXISTS `access_group` (
  `access_group_value` varchar(50) NOT NULL,
  PRIMARY KEY (`access_group_value`),
  UNIQUE KEY `access_group_id_UNIQUE` (`access_group_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. applicant
DROP TABLE IF EXISTS `applicant`;
CREATE TABLE IF NOT EXISTS `applicant` (
  `applicant_id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_guest_sciper` varchar(100) NOT NULL,
  `applicant_maturity` tinyint(4) NOT NULL,
  `applicant_gender` varchar(10) NOT NULL,
  `applicant_name` varchar(200) NOT NULL,
  `applicant_fsname` varchar(200) NOT NULL,
  `applicant_address_street` varchar(200) NOT NULL,
  `applicant_address_npa` varchar(200) NOT NULL,
  `applicant_home_phone` varchar(200) NOT NULL,
  `applicant_personal_phone` varchar(200) NOT NULL,
  `applicant_mail` varchar(200) NOT NULL,
  `applicant_birthdate` varchar(45) NOT NULL,
  `applicant_origin` varchar(100) NOT NULL,
  `applicant_nationality` varchar(100) NOT NULL,
  `applicant_foreign_authorization` varchar(10) DEFAULT NULL,
  `applicant_avs` varchar(45) NOT NULL,
  `applicant_main_language` varchar(100) NOT NULL,
  `applicant_speaks_french` tinyint(4) NOT NULL,
  `applicant_speaks_german` tinyint(4) NOT NULL,
  `applicant_speaks_english` tinyint(4) NOT NULL,
  `applicant_speaks_other` tinyint(4) NOT NULL,
  `applicant_has_majority` tinyint(4) NOT NULL,
  `applicant_scolarity_end` varchar(45) NOT NULL,
  `applicant_already_applicant` tinyint(4) NOT NULL,
  `applicant_already_applicant_year` varchar(45) DEFAULT NULL,
  `applicant_application_date` datetime NOT NULL,
  `applicant_application_updated_date` datetime DEFAULT NULL,
  `fk_applicant_main_responsible` int(11) DEFAULT NULL,
  `fk_applicant_sec_responsible` int(11) DEFAULT NULL,
  `fk_status` varchar(30) NOT NULL,
  `fk_position` int(11) NOT NULL,
  PRIMARY KEY (`applicant_id`),
  KEY `fk_applicant_responsible_idx` (`fk_applicant_main_responsible`),
  KEY `fk_applicant_responsible1_idx` (`fk_applicant_sec_responsible`),
  KEY `fk_applicant_status1_idx` (`fk_status`),
  KEY `fk_applicant_position1_idx` (`fk_position`),
  CONSTRAINT `fk_applicant_position1` FOREIGN KEY (`fk_position`) REFERENCES `position` (`position_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_applicant_responsible` FOREIGN KEY (`fk_applicant_main_responsible`) REFERENCES `responsible` (`responsible_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_applicant_responsible1` FOREIGN KEY (`fk_applicant_sec_responsible`) REFERENCES `responsible` (`responsible_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_applicant_status1` FOREIGN KEY (`fk_status`) REFERENCES `status` (`status_value`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=561 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. comment
DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_owner_sciper` varchar(45) NOT NULL,
  `comment_content` varchar(1000) NOT NULL,
  `comment_is_private` tinyint(4) NOT NULL,
  `comment_date` datetime NOT NULL,
  `fk_applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `fk_comment_applicant1_idx` (`fk_applicant_id`),
  CONSTRAINT `fk_comment_applicant1` FOREIGN KEY (`fk_applicant_id`) REFERENCES `applicant` (`applicant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. file
DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(200) NOT NULL,
  `file_path` varchar(500) NOT NULL,
  `fk_applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `fk_file_applicant1_idx` (`fk_applicant_id`),
  CONSTRAINT `fk_file_applicant1` FOREIGN KEY (`fk_applicant_id`) REFERENCES `applicant` (`applicant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3618 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. has_access
DROP TABLE IF EXISTS `has_access`;
CREATE TABLE IF NOT EXISTS `has_access` (
  `has_access_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_access_group` varchar(50) NOT NULL,
  `fk_position` int(11) NOT NULL,
  PRIMARY KEY (`has_access_id`),
  KEY `fk_has_access_access_group1_idx` (`fk_access_group`),
  KEY `fk_has_access_position1_idx` (`fk_position`),
  CONSTRAINT `fk_has_access_access_group1` FOREIGN KEY (`fk_access_group`) REFERENCES `access_group` (`access_group_value`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_has_access_position1` FOREIGN KEY (`fk_position`) REFERENCES `position` (`position_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. job
DROP TABLE IF EXISTS `job`;
CREATE TABLE IF NOT EXISTS `job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_short_value` varchar(45) NOT NULL,
  `job_full_value` varchar(100) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. last_connection
DROP TABLE IF EXISTS `last_connection`;
CREATE TABLE IF NOT EXISTS `last_connection` (
  `last_connection_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_connection_sciper` varchar(10) NOT NULL,
  `last_connection_date` datetime NOT NULL,
  `last_connection_tmp_date` datetime DEFAULT NULL,
  PRIMARY KEY (`last_connection_id`),
  UNIQUE KEY `last_connection_sciper_UNIQUE` (`last_connection_sciper`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. location
DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_site` varchar(100) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. marker
DROP TABLE IF EXISTS `marker`;
CREATE TABLE IF NOT EXISTS `marker` (
  `marker_id` int(11) NOT NULL AUTO_INCREMENT,
  `marker_owner_sciper` varchar(45) NOT NULL,
  `marker_value` int(11) NOT NULL,
  `fk_applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`marker_id`),
  KEY `fk_marker_applicant1_idx` (`fk_applicant_id`),
  CONSTRAINT `fk_marker_applicant1` FOREIGN KEY (`fk_applicant_id`) REFERENCES `applicant` (`applicant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. position
DROP TABLE IF EXISTS `position`;
CREATE TABLE IF NOT EXISTS `position` (
  `position_id` int(11) NOT NULL AUTO_INCREMENT,
  `position_spot_number` int(11) NOT NULL DEFAULT '0',
  `fk_location` int(11) NOT NULL,
  `fk_job` int(11) NOT NULL,
  PRIMARY KEY (`position_id`),
  KEY `fk_position_location1_idx` (`fk_location`),
  KEY `fk_position_job1_idx` (`fk_job`),
  CONSTRAINT `fk_position_job1` FOREIGN KEY (`fk_job`) REFERENCES `job` (`job_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_position_location1` FOREIGN KEY (`fk_location`) REFERENCES `location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. professional_activity
DROP TABLE IF EXISTS `professional_activity`;
CREATE TABLE IF NOT EXISTS `professional_activity` (
  `professional_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `professional_activity_company` varchar(100) DEFAULT NULL,
  `professional_activity_location` varchar(200) DEFAULT NULL,
  `professional_activity_activity` varchar(100) DEFAULT NULL,
  `professional_activity_years` varchar(50) DEFAULT NULL,
  `fk_applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`professional_activity_id`),
  KEY `fk_professional_activity_applicant1_idx` (`fk_applicant_id`),
  CONSTRAINT `fk_professional_activity_applicant1` FOREIGN KEY (`fk_applicant_id`) REFERENCES `applicant` (`applicant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. responsible
DROP TABLE IF EXISTS `responsible`;
CREATE TABLE IF NOT EXISTS `responsible` (
  `responsible_id` int(11) NOT NULL AUTO_INCREMENT,
  `responsible_gender` varchar(10) DEFAULT NULL,
  `responsible_name` varchar(200) DEFAULT NULL,
  `responsible_fsname` varchar(200) DEFAULT NULL,
  `responsible_street` varchar(100) DEFAULT NULL,
  `responsible_npa` varchar(100) DEFAULT NULL,
  `responsible_phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`responsible_id`)
) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. scolarity
DROP TABLE IF EXISTS `scolarity`;
CREATE TABLE IF NOT EXISTS `scolarity` (
  `scolarity_id` int(11) NOT NULL AUTO_INCREMENT,
  `scolarity_school` varchar(200) DEFAULT NULL,
  `scolarity_location` varchar(200) DEFAULT NULL,
  `scolarity_level` varchar(100) DEFAULT NULL,
  `scolarity_years` varchar(100) DEFAULT NULL,
  `fk_applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`scolarity_id`),
  KEY `fk_scolarity_applicant1_idx` (`fk_applicant_id`),
  CONSTRAINT `fk_scolarity_applicant1` FOREIGN KEY (`fk_applicant_id`) REFERENCES `applicant` (`applicant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1310 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. status
DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `status_value` varchar(30) NOT NULL,
  PRIMARY KEY (`status_value`),
  UNIQUE KEY `value_UNIQUE` (`status_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table canap_db. training
DROP TABLE IF EXISTS `training`;
CREATE TABLE IF NOT EXISTS `training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `training_job` varchar(100) DEFAULT NULL,
  `training_company` varchar(100) DEFAULT NULL,
  `fk_applicant_id` int(11) NOT NULL,
  PRIMARY KEY (`training_id`),
  KEY `fk_training_applicant1_idx` (`fk_applicant_id`),
  CONSTRAINT `fk_training_applicant1` FOREIGN KEY (`fk_applicant_id`) REFERENCES `applicant` (`applicant_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=953 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS 'email_template';
CREATE TABLE IF NOT EXISTS `email_template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `content` text DEFAULT NULL,
  PRIMARY KEY (`template_id`)
);

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
