# Changelog

All notable changes to this project will be documented in this file.

## [0.11.0] -  March 18, 2024
- Template email management

## [0.10.0] -  February 23, 2024
- New status added : "en entretien"

## [0.9.0] -  February 21, 2024
- Responsible are now able to add a BCC and a CC to their e-mail

## [0.8.0] -  January 17, 2024
- Added a Changelog
- Rewrite BCC and recipient

## [0.6.0] - January 17, 2024
  - Gest & Form mail configuration and logs 
  - installed the MSMTP configuration in the dockerfile of gest - Created a new file to be able to write mail logs on (dockerfile -> gest & form)
  - Fix the 'action' column.

## [0.5.0] - December 20, 2023
- Links for aptitude tests added in the form
- Headers are now consistent across all views
- Trainers no longer see the validations column
- Supervisors no longer see the notes column
- Sorting now works on all columns

## [0.4.0] - March 02, 2021
- Added anonymized data for development

## [0.3.0] - February 01, 2021
- Consolidated repositories canapEPFL and canapGest into a single Git repository
- Renamed canapEPFL to Formulaire
- Moved API to Gestion, renamed and moved Site to Gestion, moved Documentation
- Regrouped repositories canapEPFL and canapGEST into https://gitlab.epfl.ch/si-idevfsd/canapEPFL  and https://gitlab.epfl.ch/si-idevfsd/canapGEST/ on gitlab.epfl.ch

## [0.2.0] - January 21, 2021
- Created repositories https://gitlab.epfl.ch/si-idevfsd/canap and https://gitlab.epfl.ch/si-idevfsd/gestion/ on gitlab.epfl.ch

## [0.1.0] - January 06, 2021
- Retrieved repositories from c4sciences: https://c4science.ch/source/canapEpfl/ and https://c4science.ch/source/canapgest/

## [0.9.0] -  February 21, 2024
- Responsible are now able to add a BCC and a CC to their e-mail

## [0.8.0] -  January 17, 2024
- Added a Changelog
- Rewrite BCC and recipient

## [0.6.0] - January 17, 2024
  - Gest & Form mail configuration and logs 
  - installed the MSMTP configuration in the dockerfile of gest - Created a new file to be able to write mail logs on (dockerfile -> gest & form)
  - Fix the 'action' column.

## [0.5.0] - December 20, 2023
- Links for aptitude tests added in the form
- Headers are now consistent across all views
- Trainers no longer see the validations column
- Supervisors no longer see the notes column
- Sorting now works on all columns

## [0.4.0] - March 02, 2021
- Added anonymized data for development

## [0.3.0] - February 01, 2021
- Consolidated repositories canapEPFL and canapGest into a single Git repository
- Renamed canapEPFL to Formulaire
- Moved API to Gestion, renamed and moved Site to Gestion, moved Documentation
- Regrouped repositories canapEPFL and canapGEST into https://gitlab.epfl.ch/si-idevfsd/canapEPFL  and https://gitlab.epfl.ch/si-idevfsd/canapGEST/ on gitlab.epfl.ch

## [0.2.0] - January 21, 2021
- Created repositories https://gitlab.epfl.ch/si-idevfsd/canap and https://gitlab.epfl.ch/si-idevfsd/gestion/ on gitlab.epfl.ch

## [0.1.0] - January 06, 2021
- Retrieved repositories from c4sciences: https://c4science.ch/source/canapEpfl/ and https://c4science.ch/source/canapgest/
