# Architecture

**CanAp** est une application en plusieurs parties, la logique est la suivante :

1. **Canap-Form** : Permet de postuler et de modifier ses postulations.
2. **Canap-Gest** : Gère les postulations (validation, refus, commentaire, note, etc..). Permet aussi la gestion de la liste des professions et des places ouvertes ainsi que la gestion de liste des sites EPFL.
3. **Canap-API** : Donne l'accès aux données pour l'application "Canap-Gest".
4. **Canap-DB** : Stocke les données de la postulation.
5. **Serveur de stockage partagé** : Stocke les fichiers de la postulation.

## Bird's Eye View

```mermaid
graph
    A[Canap_Form] -->|CRUD| D((Base de données))
    B[Canap_Gest] -->|Demande/reçois| C[Canap_API]
    A --> |Stocke| E((Serveur de stockage))
    C --> |Stocke| E
    C --> |CRUD| D

```

### Form

```mermaid
graph TD
    A((Canap_Form)) --> B[Accueil]  
    B --> C(Se connecter)
    B --> D(Crée un compte guest)
    C --> E(Tequila)
    D --> E
    E --> F(Nouvelle postulation)
    E --> G(Gérer ses postulations)
    F --> H(Ajouter)
    G --> I(Voir)
    G --> J(Modifier)
    G --> K(Supprimer)
    H -->|Fichiers| M{Stockage partagé}
    H -->|Données| L{Base de données}
    I -->|Données| L
    J -->|Données| L
    K -->|Données| L
    I -->|Fichiers| M
    J -->|Fichiers| M
    K -->|Fichiers| M
```

## Explication

Le postulant va arriver sur une page qui lui demandera de se créer un compte guest(EPFL). Une fois connecté, il va avoir accès aux fonctionnalités suivantes :
1. Créer une postulation
2. Gérer ses postulations

Lorsque la postulation est faite, les données sont insérées dans la base de données. Les documents sont stockés sur un serveur de stockage partagé. Cela permet, grâce à l'application "Gest" :
1. Aux responsables de la formation apprentis de voir les postulations, de les trier et de les rendre disponibles pour les formateurs.
2. Aux formateurs de voir, noter, commenter et parcourir les postulations afin de recruter un candidat.

---

Ces postulations se trouvent dans la base de données et le serveur de stockage partagé. Le principe est qu'une fois la postulation faite par l'apprenti, elle est triée par les responsables de formation puis rendu disponible à l'ajout de commentaire et à la notation par les formateurs.

Le postulant peut d'ailleurs modifier sa postulation grâce à ce même principe.

---

## Technologie utilisée

### Authentification

On utilise **Tequila** qui est le service d'authentification web de l'EPFL.

## Canap_GEST

**Vue.js :** un Framework JavaScript conçu pour développer des interfaces d’application web SPA, c’est‐à‐dire à page unique.

**@vue/cli** : un outil en ligne de commande (CLI) qui permet la génération de projets Vue.js en suivant une structure de base.

**Vue Router** : Permet d'interpréter l'URL afin de générer les composants selon les routes et paramètre fournis.

**Vuex** : c'est un module pour Vue.js qui permet de simplifier la structure de la dépendance des composants Vue.js entre eux.

**Vuetify** : c'est un framework de composants CSS écrit en Vue.js.

**Axios** : c'est une librairie JavaScript qui permet d'effectuer des requêtes AJAX (XMLHttpRequest - XHR).Elle permet aussi l’interaction client‐serveur sans rafraichissement de page. Utilisé dans le projet pour les requêtes vers l'API.

## Canap_API

### Token

**Firebase/php-jwt** : libraire qui permet de transmettre des informations de manière sécurisée et de faire office d'authentification du client lors des requêtes à l'API.

### Serveur

**Node.js** sera utilisé dans le projet surtout pour gestionnaire de paquets : NPM.

### Modèle-vue-contrôleur

#### Composer

c'est un gestionnaire de paquets pour PHP. Utilisé dans ce projet afin d'installer le framework Lumen et ses dépendances.

#### Lumen

* Un modèle (Model) contient les données à afficher.
* Une vue (View) contient la présentation de l'interface graphique.
* Un contrôleur (Controller) contient la logique concernant les actions effectuées par l'utilisateur.

Ici, nous utilisons Lumen qui est un micro-Framework basé sur Laravel, qui n'utilise pas les éléments de la partie "view".

## Canap_Form

### PHP

**PHP: Hypertext Preprocessor**, plus connu sous son sigle PHP, est un langage de programmation libre, principalement utilisé pour produire des pages Web dynamiques via un serveur HTTP.

## Canap_DB

**MySQL** : est un système de gestion de bases de données relationnelles (SGBDR).
