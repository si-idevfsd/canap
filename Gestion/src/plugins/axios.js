import axios from 'axios'
import store from '@/store/store.js'
import router from '@/router/index.js'
import config from '../config'

const instance = axios.create({
  headers: { 'Authorization': "Bearer " + localStorage.getItem('stored_token') },
  // baseURL: 'https://canap-gest.epfl.ch/api'
  // baseURL: 'http://localhost:8181/api'
  baseURL: config.API_LOCATION
})

instance.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (!error.response) {
    store.commit('moduleSnackbar/toggle', { open: true, message: 'API non atteignable', type: 'warning' }, { root: true })
  } else if (error.response.status == 400 || error.response.status == 401) {
    // Erreur de token
    if (error.response.data.type === 'expired') {
      // Session expirée
      // store.commit('moduleSnackbar/toggle', { open: true, message: error.response.data.error, type: 'warning' }, { root: true })
      store.dispatch('moduleUser/logout', false)
    }
  } else if (error.response.status == 403 || error.response.status == 404) {
    router.push({ name: 'error', params: { status: error.response.data.error, message: error.response.data.message, route: '' } })
  } else {
    // Affichage des erreurs (snackbar)
    Object.values(error.response.data).forEach(errMsg => {
      store.commit('moduleSnackbar/toggle', { open: true, message: errMsg[0], type: 'error' }, { root: true })
    })
  }
  return Promise.reject(error)
})

export default instance