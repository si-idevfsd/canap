import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes.js'
import store from '../store/store.js'

Vue.use(Router)

const router = new Router({ routes })

router.beforeEach((to, from, next) => {
  if (store.getters['moduleUser/userIsLogedIn']) {
    // get & set user data
    return store.dispatch('moduleUser/getUserData').then(() => {
      // Valide l'accès à la route selon le rôle
      let routeLimitation = to.meta.requiresRole
      let userRole = store.state.moduleUser.userData.role

      if (routeLimitation == 'responsable' && userRole == 'formateur') {
        // Routes limitées aux responsables
        let deniedRoutes = ['/openjobs']
        if (deniedRoutes.includes(to.fullPath)) {
          return next({ name: 'error', params: { status: 403, route: to.path, message: 'Accès non autorisé' } })
        }
      }
      if (routeLimitation == 'formateur' && userRole == 'responsable') {
        // Routes limitées aux formateurs
        let deniedRoutes = ['/favourites']
        if (deniedRoutes.includes(to.fullPath)) {
          return next({ name: 'error', params: { status: 403, route: to.path, message: 'Accès non autorisé' } })
        }
      }
      return next()
    })
  }
  next()
})

export default router
