const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/HomeView.vue')
  },
  {
    path: '/applications',
    name: 'applications',
    component: () => import('@/views/ApplicationsView.vue')
  },
  {
    path: '/application/:id',
    name: 'application',
    component: () => import('@/views/ApplicationView.vue')
  },
  {
    path: '/openjobs',
    name: 'openjobs',
    component: () => import('@/views/OpenJobsView.vue'),
    meta: { requiresRole: 'responsable' }
  },
  {
    path: '/favourites',
    name: 'favourites',
    component: () => import('@/views/FavouritesView.vue'),
    meta: { requiresRole: 'formateur' }
  },
  {
    path: '/error',
    name: 'error',
    props: true,
    component: () => import('@/views/ErrorView.vue')
  },
  {
    path: '*',
    name: 'notfound',
    redirect: { name: 'error', params: { status: 404 } },
    component: () => import('@/views/ErrorView.vue')
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import('@/views/SettingsView.vue'),
    meta: { requiresRole: 'responsable' }
  },
  {
    path: '/settings/email-templates',
    name: 'settings',
    component: () => import('@/views/EmailTemplatesView.vue'),
    meta: { requiresRole: 'responsable' }
  },
]

export default routes