let preset_texts = [
  {
    title: "ACCUSE DE RECEPTION",
    content: "Votre candidature pour une place d’apprentissage à l’EPFL\n\rNous venons d’enregistrer votre candidature et vous remercions de votre intérêt pour la formation professionnelle à l’Ecole polytechnique fédérale de Lausanne.\n\rNous allons étudier votre dossier avec la plus grande attention et nous ne manquerons pas de vous contacter si votre profil répond à nos attentes.\n\rAvec nos meilleures salutations.\n\rFormation Apprentis EPFL"
  },
  {
    title: "NON APRES EXAMEN DOSSIER DE CANDIDATURE",
    content: "Votre candidature pour une place d’apprentissage à l’EPFL\n\rNous nous référons à votre candidature pour un apprentissage de [METIER] dans notre Ecole.\n\rAprès examen de votre dossier, nous sommes au regret de vous informer que votre candidature n’a pas été retenue, d’autres candidats répondant d’avantage au profil recherché.\n\rNous vous souhaitons plein succès dans vos recherches d’une place d’apprentissage et vous adressons nos meilleures salutations.\n\rFormation Apprentis EPFL"
  },
  {
    title: "NON APRES ENTRETIEN / STAGE / TEST PRATIQUE",
    content: "Votre candidature pour une place d’apprentissage à l’EPFL\n\rNous nous référons à votre candidature pour un apprentissage de [METIER] dans notre Ecole.\n\rNous avons malheureusement le regret de vous informer qu’après votre entretien personnel avec nos formateurs en entreprise (suite au test d’aptitudes pratiques que vous avez effectué dans notre Ecole), votre candidature n’a finalement pas été retenue, d’autres candidats répondant d’avantage au profil recherché.\n\rNous vous souhaitons plein succès dans vos recherches d’une place d’apprentissage et vous adressons nos meilleures salutations.\n\rFormation Apprentis EPFL"
  },
  {
    title: "NON FINAL",
    content: "Votre candidature pour une place d’apprentissage à l’EPFL\n\rNous nous référons à votre candidature pour un apprentissage de [METIER] dans notre Ecole et vous remercions pour les démarches que vous avez entreprises dans le cadre de notre procédure de recrutement de nos futurs apprentis et apprenties.\n\rNous avons malheureusement le regret de vous informer que nous n’avons finalement pas retenu votre candidature, d’autres candidats répondant d’avantage au profil recherché.\n\rNous vous souhaitons plein succès dans vos recherches d’une place d’apprentissage et vous adressons nos meilleures salutations.\n\rFormation Apprentis EPFL"
  },
  {
    title: "FIN SELECTION",
    content: "Votre candidature pour une place d’apprentissage à l’EPFL\n\rNous avons bien reçu votre candidature pour un apprentissage de [METIER] et vous remercions de votre intérêt pour la formation professionnelle à l’Ecole polytechnique fédérale de Lausanne.\n\rEn réponse nous devons toutefois vous informer que nous avons aujourd’hui terminé la sélection de nos futur-e-s apprenti-e-s pour cette profession.\n\rNous vous souhaitons plein succès dans vos recherches d’une place d’apprentissage et vous adressons nos meilleures salutations.\n\rFormation Apprentis EPFL"
  },
  {
    title: "DOSSIER INCOMPLET",
    content: "Votre candidature pour une place d’apprentissage à l’EPFL\n\rNous avons bien reçu votre candidature pour un apprentissage de [METIER] et vous remercions de votre intérêt pour la formation professionnelle à l’Ecole polytechnique fédérale de Lausanne.\n\rVotre dossier de candidature étant incomplet, nous ne pouvons malheureusement y donner une suite favorable.\n\rNous vous souhaitons plein succès dans vos recherches d’une place d’apprentissage et vous adressons nos meilleures salutations.\n\rFormation Apprentis EPFL"
  }
]
export default preset_texts