export function setPositions (state, data) {
  state.positions = data
}

export function setJobs (state, data) {
  state.available_jobs = data
}

export function setLocations (state, data) {
  state.available_locations = data
}

export function setAccessGroups (state, data) {
  state.available_access_groups = data
}