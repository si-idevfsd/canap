import axios from '../../../plugins/axios'

export function getPositions (context) {
  axios({
    method: 'get',
    url: '/positions'
  })
    .then(response => {
      context.commit('setPositions', response.data)
    })
}

export function getJobs (context) {
  axios({
    method: 'get',
    url: '/jobs'
  })
    .then(response => {
      context.commit('setJobs', response.data)
    })
}

export function getLocations (context) {
  axios({
    method: 'get',
    url: '/locations'
  })
    .then(response => {
      context.commit('setLocations', response.data)
    })
}

export function getAccessGroups (context) {
  axios({
    method: 'get',
    url: '/groups'
  })
    .then(response => {
      context.commit('setAccessGroups', response.data)
    })
}

export function createPosition (context, data) {
  data.new_groups = []
  data.access_group_cleared = []
  data.access_groups.forEach(group => {
    if (!group.access_group_value) {
      data.new_groups.push(group)
    } else {
      data.access_group_cleared.push(group)
    }
  })

  axios({
    method: 'put',
    url: '/position',
    data: data
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function updatePosition (context, data) {
  data.new_groups = []
  data.access_group_cleared = []
  data.access_groups.forEach(group => {
    if (!group.access_group_value) {
      data.new_groups.push(group)
    } else {
      data.access_group_cleared.push(group)
    }
  })
  return axios({
    method: 'patch',
    url: '/position/' + data.position_id,
    data: data
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function deletePosition (context, data) {
  return axios({
    method: 'delete',
    url: '/position/' + data.position_id
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function createJob (context, data) {
  return axios({
    method: 'put',
    url: '/job',
    data: data
  })
    .then(response => {
      return response.data
    })
}

export function createLocation (context, data) {
  return axios({
    method: 'put',
    url: '/location',
    data: data
  })
    .then(response => {
      return response.data
    })
}