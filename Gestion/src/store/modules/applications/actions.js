import axios from '../../../plugins/axios'

export function getApplications (context) {
  axios({
    method: 'get',
    url: '/applicants'
  })
    .then(response => {
      context.commit('setApplications', response.data)
    })
}

export function getCurrentApplicationData (context, data) {
  return axios({
    method: 'get',
    url: '/applicant/' + data.id
  })
    .then(response => {
      context.commit('setCurrentApplicationData', response.data)
    })
}

export function getCurrentApplicationComments (context, data) {
  return axios({
    method: 'get',
    url: '/applicant/' + data.id + '/comments'
  })
    .then(response => {
      let results = {
        public: [],
        private: []
      }
      response.data.public.forEach(comment => {
        getOwnerData(comment.comment_owner_sciper).then(res => {
          comment.ownerData = res
          results.public.push(comment)
        })
      })

      response.data.private.forEach(comment => {
        getOwnerData(comment.comment_owner_sciper).then(res => {
          comment.ownerData = res
          results.private.push(comment)
        })
      })
      context.commit('setCurrentApplicationComments', results)
    })
}

export function getCurrentApplicationMarker (context, data) {
  return axios({
    method: 'get',
    url: '/applicant/' + data.id + '/marker'
  })
    .then(response => {
      context.commit('setCurrentApplicationMarker', response.data.marker)
    })
}

export function getAvailableStatus (context) {
  axios({
    method: 'get',
    url: '/status'
  })
    .then(response => {
      context.commit('setAvailableStatus', response.data)
    })
}

export function changeApplicationStatus (context, data) {
  axios({
    method: 'patch',
    url: '/status/applicant/' + data.id,
    data: data
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function deleteApplication (context, data) {
  axios({
    method: 'delete',
    url: '/applicant/' + data.id,
    data: data
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function getFile (context, data) {
  axios({
    method: 'get',
    url: '/file/' + data.id,
    responseType: 'blob'
  })
    .then(response => {
      generateBlob(response, data.name)
    })
}

export function getAllFiles (context, data) {
  axios({
    method: 'get',
    url: '/files/applicant/' + data.id,
    responseType: 'blob'
  })
    .then(response => {
      generateBlob(response, data.id + 'export-annexes.zip')
    })
}

function getOwnerData (sciper) {
  return axios({
    method: 'get',
    url: '/user/data/' + sciper
  })
    .then(response => {
      return response.data
    })
}

export function exportData (context, data) {
  axios({
    method: 'get',
    url: '/applicant/' + data.id + '/export',
    responseType: 'blob'
  })
    .then(response => {
      generateBlob(response, 'export' + data.name.replace(/\s/g, '') + '.zip')
    })
}

function generateBlob (response, filename) {
  const url = window.URL.createObjectURL(new Blob([response.data]))
  const link = document.createElement('a')
  link.href = url
  link.setAttribute('download', filename)
  document.body.appendChild(link)
  link.click()
}

export function createMarker (context, data) {
  axios({
    method: 'put',
    url: '/marker',
    data: {
      value: data.value,
      applicant_id: data.applicant
    }
  })
}

export function updateMarker (context, data) {
  axios({
    method: 'patch',
    url: '/marker/' + data.id,
    data: {
      value: data.value,
      applicant_id: data.applicant
    }
  })
}

export function deleteMarker (context, data) {
  axios({
    method: 'delete',
    url: '/marker/' + data.id
  })
}

export function addComment (context, data) {
  return axios({
    method: 'put',
    url: '/comment',
    data: {
      applicant_id: data.applicant_id,
      content: data.content,
      is_private: data.is_private
    }
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function deleteComment (context, data) {
  return axios({
    method: 'delete',
    url: '/comment/' + data.id
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function updateComment (context, data) {
  return axios({
    method: 'patch',
    url: '/comment/' + data.id,
    data: {
      content: data.content,
      is_private: data.isPrivate
    }
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
}

export function sendMails (context, data) {
   console.log("test")
  return axios({
    method: 'post',
    url: '/mail/applicant',
    data: {
      recipients: data.to,
      bcc: data.bcc,
      cc: data.cc,
      jobs: data.jobs,
      subject: data.subject,
      content: data.content
    }
  })
  .then(response => {
    if (response.data.success) {
      console.log(response.data)
      context.commit('moduleSnackbar/toggle', { open: true, message: 'Mails envoyés avec succès', type: 'success' }, { root: true });
    } else {
      const errorMessage = response.data.message || 'Une erreur s\'est produite lors de l\'envoi des mails';
      context.commit('moduleSnackbar/toggle', { open: true, message: errorMessage, type: 'error' }, { root: true });
      throw new Error(errorMessage);
    }
  })
}

export function getEmailTemplates(context) {
  console.log("is called")
  return axios({
    method: 'get',
    url: '/mail/templates',
  })
  .then(response => {
    context.commit('setTemplates', response.data)
    // console.log("this is getemailtemplate:", response.data)
  })
    .catch(error => {
      context.commit('moduleSnackbar/toggle', { open: true, message: error.response.data.message, type: 'error' }, { root: true });
      return [];
    });
}

export function addEmailTemplate (context, data) {
  return axios({
    method: 'put',
    url: '/mail/templates/new',
    data: {
      title: data.title,
      subject: data.subject,
      content: data.content
    }
  })
    .then(response => {
      context.commit('moduleSnackbar/toggle', { open: true, message: response.data.message, type: 'success' }, { root: true })
    })
  }

  export function deleteEmailTemplate (context, data) {
    return axios({
      method: 'delete',
      url: 'mail/templates/delete/' + data.template_id
    })
      .then(() => {
        context.commit('moduleSnackbar/toggle', { open: true, message: "Template supprimé avec succès", type: 'success' }, { root: true });
      })
        .catch(error => {
          console.error("Une erreur s'est produite lors de la suppression du template :", error);
          context.commit('moduleSnackbar/toggle', { open: true, message: "Une erreur s'est produite lors de la suppression du template", type: 'error' }, { root: true });
      });
  }
  
  export function updateEmailTemplate (context, data) {
    return axios({
      method: 'patch',
      url: '/mail/templates/update/' + data.template_id,
      data: {
        title: data.title,
        subject: data.subject,
        content: data.content
      }
    })
      .then(() => {
        context.commit('moduleSnackbar/toggle', { open: true, message: "Modèle modifié avec succès", type: 'success' }, { root: true });
      })
        .catch(error => {
          console.error("Une erreur s'est produite lors de la mise à jour du modèle :", error);
          context.commit('moduleSnackbar/toggle', { open: true, message: "Une erreur s'est produite lors de la mise à jour du modèle", type: 'error' }, { root: true });
      });
  }
