export function setApplications (state, data) {
  state.applications = data
}

export function setCurrentApplicationData (state, data) {
  state.currentApplication.application_data = data
}

export function setCurrentApplicationComments (state, data) {
  state.currentApplication.comments = data
}

export function setCurrentApplicationMarker (state, data) {
  state.currentApplication.marker = data
}

export function setAvailableStatus (state, data) {
  state.available_status = data
}

export function setFilterJob (state, data) {
  state.filters.selectedJob = data
}

export function setFilterStatus (state, data) {
  state.filters.selectedStatus = data
}

export function setFilterSearch (state, data) {
  state.filters.search = data
}

export function setMailDialogOpened (state, data) {
  state.mail_dialog_opened = data
}

export function setTemplates (state, data) {
  state.templates = data
}