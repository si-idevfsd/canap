export function getFilteredApplications(state) {
  if (!state.filters.selectedJob && !state.filters.selectedStatus) {
    return state.applications
  } else if (state.filters.selectedJob && !state.filters.selectedStatus) {
    return state.applications.filter(x => x.job_short_value === state.filters.selectedJob)
  } else if (!state.filters.selectedJob && state.filters.selectedStatus) {
    return state.applications.filter(x => x.fk_status === state.filters.selectedStatus)
  } else {
    return state.applications.filter(x => x.job_short_value === state.filters.selectedJob && x.fk_status == state.filters.selectedStatus)
  }
}

export function getFormatedApplicationDate(state) {
  let dateTimeParts = state.currentApplication.application_data.personal_data.applicant_application_date.split(/[- :]/)
  dateTimeParts[1]--
  let date = new Date(...dateTimeParts)
  return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' à ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2)
}

export function getFormatedApplicationUpdateDate(state) {
  if (state.currentApplication.application_data.personal_data.applicant_application_updated_date) {
    let dateTimeParts = state.currentApplication.application_data.personal_data.applicant_application_updated_date.split(/[- :]/)
    dateTimeParts[1]--
    let date = new Date(...dateTimeParts)
    return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' à ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2)
  } else {
    return null
  }
}

export function getResponsibleNewApplications(state) {
  return state.applications.filter(x => x.fk_status == 'Nouveau')
}

export function getFormateurNewApplications(state, getters, rootState) {
  return state.applications.filter(x => x.applicant_application_date > rootState.moduleUser.lastConnection.last_connection_date)
}

export function getSortedPublicComments(state) {
  return state.currentApplication.comments.public.sort((a, b) => new Date(b.comment_date) - new Date(a.comment_date))
}

export function getSortedPrivateComments(state) {
  return state.currentApplication.comments.private.sort((a, b) => new Date(b.comment_date) - new Date(a.comment_date))
}

export function getTemplates(state){
  return state.templates
}