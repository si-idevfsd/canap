export default {
  applications: [],
  currentApplication: {
    application_data: {},
    comments: {},
    marker: {}
  },
  available_status: [],
  filters: {
    selectedJob: null,
    selectedStatus: null,
    search: ''
  },
  templates: [],
}