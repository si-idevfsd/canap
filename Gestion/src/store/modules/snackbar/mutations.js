export function toggle (state, data) {
  state.open = data.open
  state.message = data.message
  state.type = data.type
}