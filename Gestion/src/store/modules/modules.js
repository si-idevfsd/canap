import moduleUser from './user'
import moduleApplications from './applications'
import modulePositions from './positions'
import moduleSnackbar from './snackbar'

export default {
  moduleUser,
  moduleApplications,
  modulePositions,
  moduleSnackbar
}