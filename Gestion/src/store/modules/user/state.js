export default {
  userData: {
    tequila: {
      firstname: '',
      name: '',
      group: String,
      user: String,
      sciper: String
    },
    role: '',
    permissions: []
  },
  permittedJobs: [],
  lastConnection: {}
}