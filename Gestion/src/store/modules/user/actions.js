import axios from '../../../plugins/axios'
import config from '../../../config'

export function login (context, key) {
  if (key) {
    axios.get("/auth/login?key=" + key).then(response => {
      localStorage.setItem('stored_token', response.data)
      location.reload()
    })
  }
  else {
    //window.location.href = "https://canap-gest.epfl.ch/api/auth/login"
    //window.location.href = "http://localhost:8181/api/auth/login"
    window.location.href = config.LOGIN_REDIRECT
  }
}

export function logout (context, fullLogout) {
  localStorage.removeItem('stored_token')
  if (fullLogout) {
    setLastConnection().then(() => {
      window.location = 'https://tequila.epfl.ch/logout'
    })
  } else {
    location.reload()
  }
}

export function getUserData (context) {
  return axios({
    method: 'get',
    url: '/user'
  })
    .then(response => {
      context.commit('setUserData', response.data)
      return response.data
    })
}

export function getUserPermittedJobs (context) {
  axios({
    method: 'get',
    url: '/user/permittedjobs'
  })
    .then(response => {
      context.commit('setPermittedJobs', response.data)
    })
}

export function getLastConnection (context) {
  axios({
    method: 'get',
    url: '/user/connection'
  })
    .then(response => {
      context.commit('setLastConnection', response.data.last_connection)
    })
}

export function setLastConnection () {
  return axios({
    method: 'patch',
    url: '/user/connection'
  })
}