export function setUserData (state, data) {
  state.userData = data
}

export function setPermittedJobs (state, data) {
  state.permittedJobs = data
}

export function setLastConnection (state, data) {
  state.lastConnection = data
}