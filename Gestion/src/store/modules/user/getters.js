export function userIsLogedIn() {
  return localStorage.getItem('stored_token') != null
}

export function userIsResponsible(state) {
  return state.userData.role === 'responsable'
}