module.exports = {
  devServer: {
    port: 8080,
    https: false,
    disableHostCheck: true,
  },
  publicPath: "/gest"
}