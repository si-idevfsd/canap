<!---

                         Merci de lire ceci !

Avant d'ouvrir un nouveau bug, soyez sûr d'avoir fait une recherche sur
https://gitlab.epfl.ch/si-idevfsd/canap/issues et vérifié que vous n'êtes pas 
en train de soumettre un doublon.

Merci de préfixer votre titre avec "[bug]" pour plus de lisibilité.

Vous pouvez modifier ce modèle à votre guise, mais soyez sûr de donner le 
maximum d'information utiles pour que les développeurs soient en mesure de 
comprendre, reproduire et éventuellement réparer.

--->

### Résumé

<!-- 
  Résumez le bug rencontré de manière concise.
-->

### Étapes pour reproduire le bug

<!-- 
  Décrivez comment on peut reproduire le bug — c'est très important que vous
  soyez précis. Merci d'utiliser une liste ordonnée et de mentionner les URLs.

  Exemple :

  1. Lors que je navigue sur la page https://...
  2. Et que je clique sur le lien intitulé ...
  3. Alors j'obtiens une page 404 à l'URL suivante: https://...
-->

### Comportement

<!--
  * Quel est le comportement actuel du *bug* ?
  * Quel est le resultat *correct* attendu ?
-->

### Informations supplémentaires

<!-- 
  Merci de coller ici les logs (utiliser les bloque de code (```) pour les
  mettre en page) et/ou les captures d'écran décrivant le problème.
-->

### Solutions possibles

<!-- 
  Si vous avez identifiez le fichier ou le code responsable du problème, vous
  pouvez le mentionner ici.
  Si le comportement de l'application doit être modifié, vous pouvez exposer 
  vos propositions ici.
-->

<!-- Ajoute le label "Bug" automatiquement -->
/label ~Bug
/assign me
