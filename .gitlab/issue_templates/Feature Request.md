<!---

                         Merci de lire ceci !

Avant de demander une nouvelle fonctionnalité, soyez sûr d'avoir fait une
recherche sur https://gitlab.epfl.ch/si-idevfsd/canap/issues et vérifié que vous
n'êtes pas en train de soumettre un doublon.

Merci de préfixer votre titre avec "[fr]" pour plus de lisibilité.

Vous pouvez modifier ce modèle à votre guise, mais soyez sûr de donner le 
maximum d'information utiles pour que les développeurs soient en mesure de 
comprendre votre proposition.

--->

### Résumé

<!-- 
  Résumez la proposition rencontré de manière concise.
-->

### Description détaillée

<!-- 
  Décrivez ici ce que vous attendez avec cette proposition de fonctionnalité.
  Mentionnez sur quelles pages vous souhaiteriez voir cette fonctionnalité, 
  quelles sont les fonctionnalités attendues, les bénéfices de l'ajout de cette
  fonctionnalité, etc.
-->

### Informations supplémentaires

<!-- 
  Si vous avez un schéma ou une capture d'écran permettant de décrire plus en 
  détail la fonctionnalité, vous pouvez l'ajouter ici.
-->


<!-- Ajoute le label "Feature Request" automatiquement -->
/label ~"Feature Request"
/assign me
