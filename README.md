# CanAp — Candidature Apprentis

Ce projet regroupe les différentes parties du site des candidatures des apprentis. Son architecture est disponible [ici](ARCHITECTURE.md).

Il est organisé de la manière suivante :
   - API → API pour le site de management
   - data → Pour le stockage local (scripts d'initialisation SQL, volumes Docker et fichiers de candidatures)
   - Docker → Les fichiers relatifs à l'utilisation de Docker
   - Documentation → Documentation et TPI de Nicolas Crausaz
   - Formulaire → Formulaire de postulation
   - Gestion → Site de management des postulations (pour formateurs et admin)

## Pour le développement avec Docker

### Mise en place

1) Commencez par créer les fichiers de configuration :
  - Copiez `Formulaire/configs/config.template.php` vers `Formulaire/configs/config.php` ;
  - Copiez `API/.env.example` vers `API/.env` ;
  - Si nécessaire, modifiez les entrées de ces fichiers selon vos besoins.
  - En particulier : `TEQUILA_REQUIRE_PARAMS="groupe EPFL"`

2) Vous pouvez ensuite déployer les applications avec
`docker-compose -f Docker/docker-compose-dev.yml up -d`.

3) Après avoir lancé les containers (et attendu qu'ils soient construits), 
installez les dépendances :
  - `docker exec -it canap_gest sh -c 'npm i --no-funding'` ;
  - `docker exec -it canap_api bash -c 'composer update && composer install'` ;

4) Rendez-vous sur
  - http://localhost:8180 pour le formulaire d'application ;
  - http://localhost:8080 pour l'application de gestion ;
  - http://localhost:8181 pour l'API.

### Accès à l'app gest
J’ai modifié le fichier .env en ajoutant un groupe dans le quel je posséde des droits :


   - il est nécessaire d'être dans le réseau de l'EPFL (par exemple avec le VPN)
     — si l'application boucle sur Tequila ou que l'API n'est pas atteignable, 
     vérifiez que vous êtes bien connecté au réseau de l'EPFL.
   - par défaut la redirection Tequila se faire en https ; après la redirection
     il faut changer manuellement le `https://` en `http://` en gardant le reste
     de l'URL.